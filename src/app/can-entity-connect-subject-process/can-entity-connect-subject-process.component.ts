import { AuthService } from './../service/auth.service';
import { SubjectService } from './../service/subject.service';
import { ListEntityService } from './../service/list-entity.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-can-entity-connect-subject-process',
  templateUrl: './can-entity-connect-subject-process.component.html',
  styles: ['']
})
export class CanEntityConnectSubjectProcessComponent implements OnInit {

  constructor(private _fb: FormBuilder, private _auth: AuthService, private _serviceEntity: ListEntityService, private _serviceSubjectProcess: SubjectService) { }

  status:string = 'creating';
  subjectProcessSelected:any;
  entityDetailSelected:any;
  registrationForm = this._fb.group({
    id_subjectProcess: [''],
    id_entity: [''],
    suggestionText: [''],
    canCreateProcess: [false],
    opinion: [''],
    dispatch: [''],
    filing: [''],
    decision: [''],
    authorization:[''],
    attach:['']
  })
  _listSubjectProcess: any;
  _listAdministrativeSibling: any;
  _listCanEntityConnectSubjectProcess: any;
  //  id_subjectProcess:string;

  ngOnInit() {
    this.listSubjectProcess();
    //   this.listAdministrativeSibling();
  }

  onChangeSubjectProcess() {
    //  this.listSubjectDocument();

    this.subjectProcessSelected = this._listSubjectProcess.find((subjectProcess)=>{
      return subjectProcess._key === this.registrationForm.value.id_subjectProcess
    }) 
    
    this.listCanEntityConnectSubjectProcess();
    this.listAdministrativeSibling();
  }

  listCanEntityConnectSubjectProcess() {

    this._serviceSubjectProcess.listCanEntityConnectSubjectProcess(this.registrationForm.value.id_subjectProcess)
      .subscribe(
        success => {
          this._listCanEntityConnectSubjectProcess = success
        },
        error => console.log("Error!", error)

      )
  }
  listSubjectProcess() {
    this._serviceSubjectProcess.listSubjectProcess('').subscribe(
      success => this._listSubjectProcess = success,
      error => console.log('Error!', error)
    )

  }

  listAdministrativeSibling() {
    let data = {
      key_entity: this._auth.getDataLogged().entity._key,
      key_subjectProcess: this.registrationForm.value.id_subjectProcess
    }
    this._serviceEntity.listAdministrativeSibling(data).subscribe(
      success => this._listAdministrativeSibling = success,
      error => console.log('Error!', error)
    )

  }

  edit(entityDetail: any) {
    this.status = 'editing';
    this.registrationForm.setValue({
      id_subjectProcess: this.registrationForm.value.id_subjectProcess,
      id_entity:entityDetail.entity._key,
      suggestionText: entityDetail.has.suggestionText,
      canCreateProcess: entityDetail.has.canCreateProcess,
      opinion: entityDetail.has._listTypeDocumentCreation.indexOf('Opinion') >= 0?'Opinion':'', 
      dispatch: entityDetail.has._listTypeDocumentCreation.indexOf('Dispatch') >= 0?'Dispatch':'',
      filing: entityDetail.has._listTypeDocumentCreation.indexOf('Filing') >= 0?'Filing':'',
      decision: entityDetail.has._listTypeDocumentCreation.indexOf('Decision') >= 0?'Decision':'',
      authorization: entityDetail.has._listTypeDocumentCreation.indexOf('Authorization') >= 0?'Authorization':'',
      attach: entityDetail.has._listTypeDocumentCreation.indexOf('Attach') >= 0?'Attach':''
      
    }
    )
    this.entityDetailSelected = entityDetail;
    
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;

    
  }
  newCan() {

    this.status = 'creating';
    this.registrationForm.setValue({
      id_subjectProcess: this.registrationForm.value.id_subjectProcess,
      id_entity:'',
      suggestionText: '',
      canCreateProcess: false,
      opinion: '', 
      dispatch: '',
      filing: '',
      decision: '',
      authorization:'',
      attach:''
    }
    )

  }

  onSubmit() {
    let _listTypeDocumentCreation = [];
    if (this.registrationForm.value.opinion == "Opinion") {
      _listTypeDocumentCreation.push("Opinion");
    }
    if (this.registrationForm.value.dispatch == "Dispatch") {
      _listTypeDocumentCreation.push("Dispatch");
    }
    if (this.registrationForm.value.filing == "Filing") {
      _listTypeDocumentCreation.push("Filing");
    }
    if (this.registrationForm.value.decision == "Decision") {
      _listTypeDocumentCreation.push("Decision");
    }
    if (this.registrationForm.value.authorization == "Authorization") {
      _listTypeDocumentCreation.push("Authorization");
    }
    if(this.registrationForm.value.canCreateProcess=="true") {
      _listTypeDocumentCreation.push("Ordinary");
    }
    if(this.registrationForm.value.attach=="Attach") {
      _listTypeDocumentCreation.push("Attach");
    }
    console.log(_listTypeDocumentCreation);
    

    let submitData:any = {
      id_subjectProcess: this.registrationForm.value.id_subjectProcess,
      id_entity: this.registrationForm.value.id_entity,
      suggestionText: this.registrationForm.value.suggestionText,
      canCreateProcess: this.registrationForm.value.canCreateProcess,
      _listTypeDocumentCreation: _listTypeDocumentCreation
    }
    if(this.status === 'creating') {
      this._serviceEntity.saveCanEntityConnectSubjectProcess(submitData).subscribe(
        sucess => {
          //        this.listSubjectProcess();
          this.listCanEntityConnectSubjectProcess();
        },
        error => console.log('Error!', error)
      )
    } else {
      submitData.key_can = this.entityDetailSelected.has._key;
      this._serviceEntity.updateCanEntityConnectSubjectProcess(submitData).subscribe(
        sucess => {
          //        this.listSubjectProcess();
          this.listCanEntityConnectSubjectProcess();
        },
        error => console.log('Error!', error)
      )

    }

    this.newCan();
    this.listCanEntityConnectSubjectProcess();
    this.listAdministrativeSibling();

  }

}
