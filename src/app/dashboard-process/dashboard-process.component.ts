import { AuthService } from './../service/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-process',
  templateUrl: './dashboard-process.component.html',
  styles: ['']
})
export class DashboardProcessComponent implements OnInit {

  constructor(private _auth:AuthService) { }

  id_entity:string;
  id_super;
  ngOnInit() {
    this.id_entity = this._auth.getDataLogged().entity._key;
    this.id_super = this._auth.getDataLogged().super._key;
  }

}
