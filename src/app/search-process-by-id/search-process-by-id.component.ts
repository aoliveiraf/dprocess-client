import { SearchProcessService } from './../service/search-process.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-process-by-id',
  templateUrl: './search-process-by-id.component.html',
  styles: ['']
})
export class SearchProcessByIdComponent implements OnInit {

  constructor(private _fb:FormBuilder,private searchService:SearchProcessService) { }

  processFullDetail:any;
  lastId=-1;

  searchForm = this._fb.group({
    id_process:['']
  })

  ngOnInit() {
  }

  showORhide(id) {
    var x = document.getElementById(id);
    
    console.log(x);
    
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  onShowText(index) {    
    
    if(this.lastId!=-1) {
      this.showORhide(this.lastId)
    }
    this.showORhide(index)
    this.lastId=index;

  }

  onSubmit() {

    this.searchService.searchProcessById(this.searchForm.value.id_process).
    subscribe(
      success => {
        this.processFullDetail = success;
    //    console.log(success);
        
      },
      error => console.log('error!',error)
    )
  }

}
