import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { EntityService } from './../service/entity.service';
import { AuthService } from './../service/auth.service';
import { ListProcessService } from './../service/list-process.service';
import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-list-process',
  templateUrl: './list-process.component.html',
  styles: ['']
})
export class ListProcessComponent implements OnInit {

  _listProcess:any;
  listSubject:any;
  id_subject:string;
  id_entity:string;
  title:string;
  constructor(private _route:ActivatedRoute, private _fb:FormBuilder, private _entityService:EntityService, private serviceListProcess:ListProcessService,private auth:AuthService) { }

  registrationForm = this._fb.group({
    id_subject:[''],
    id_process:['']
  })

  ngOnInit() {
    
    this.id_entity = this.auth.getDataLogged().entity._key;
    this.lookupSubject(this.id_entity);

  }

  onChange() {

    this.id_subject = this.registrationForm.value.id_subject;
    
    this.listProcess();

  }

  showORhide() {
    var x = document.getElementById("idButton");
    
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  listProcess() {
    let data = {id_subject:this.id_subject,id_entity:this.id_entity}
    
    this._entityService.listProcess(data)
    .subscribe(
      success => {
        this._listProcess = success;
      },
      error => console.log("Error!",error)
    )
  }


  lookupSubject(idEntity:string) {

    this._entityService.listSubjectFromProcess(idEntity).
    subscribe(
      success => this.listSubject = success,
      error => console.log("Error!", error)
    )
  }

  onClick(tabName:string) {

      this.serviceListProcess.listMyProcess(this.auth.getDataLogged().entity._key)
      .subscribe(
        success => this._listProcess=success,
        error => console.log('Error!',error)
      )
  }

  destacarId() {
    console.log(this.registrationForm.value.id_process)
    this.filterSelection(this.registrationForm.value.id_process)
  }

  filterSelection(query:string) {
    var elements, i;
    elements = document.getElementsByClassName("dinamic-show");
    
    // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
    for (i = 0; i < elements.length; i++) {
      this.removeClass(elements[i], query);
    }
  }
    
  // Hide elements that are not selected
  removeClass(element, query) {

    let index = element.className.indexOf(query);
    
    if(index < 0) {
      element.style.display="none"
    } else {
      element.style.display="block"
    }

  }


}
