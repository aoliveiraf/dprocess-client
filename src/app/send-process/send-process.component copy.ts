import { Router } from '@angular/router';
import { MovementProcessService } from './../service/movement-process.service';
import { AuthService } from './../service/auth.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { EntityService } from '../service/entity.service';

@Component({
  selector: 'app-send-process',
  templateUrl: './send-process.component.html',
  styleUrls: ['./send-process.component.css']
})
export class SendProcessComponent implements OnInit {

  _listProcess:any;
  listSubject:any;
  idEntity:string;
  idSubject:string;
  listEntityCanMoveToSubject:any;
  isShowDispatch:boolean = false;
  listIdProcessToMove = [];
  btnDisabled = true;

  constructor(private _router:Router, private _fb:FormBuilder, private _entityService:EntityService, private _movement:MovementProcessService,private _auth:AuthService) { }

  registrationForm =  this._fb.group({
    id_subject:[''],
    id_entityTo:[''],
    id_process:['']
  })
  ngOnInit() {
   this.idEntity = this._auth.getDataLogged().entity._key;
    this.lookupSubject(this.idEntity);

  }

  reset() {
    this.registrationForm.setValue({
      id_subject:this.registrationForm.get('id_subject').value,
      id_entityTo:'',
      id_process:''  
    })
    this.listIdProcessToMove=[]

  }
  lookupSubject(idEntity:string) {

    this._entityService.listSubjectFromProcess(idEntity).
    subscribe(
      success => this.listSubject = success,
      error => console.log("Error!", error)
    )
  }

  dispatch() {
    this.isShowDispatch = !this.isShowDispatch;
  }
  listProcess() {
    let data = {id_subject:this.idSubject,id_entity:this.idEntity}
    this._entityService.listProcess(data)
    .subscribe(
      success => {
        this._listProcess = success;
        this.fetchListEntityCanMoveToSubjectProcess();
      },
      error => console.log("Error!",error)
    )
  }

  onChangeCheck(id_process) {
    let index = this.listIdProcessToMove.indexOf(id_process);
    if(index==-1) {
      this.listIdProcessToMove.push(id_process);
    } else {
      this.listIdProcessToMove.splice(index,1)
    }
    if(this.listIdProcessToMove.length>0) {
      this.btnDisabled=false;
    } else {
      this.btnDisabled=true;
    }
  
  }

  fetchListEntityCanMoveToSubjectProcess() {
    let data = {id_subjectProcess:this.idSubject,id_super:this._auth.getDataLogged().super._key}
    this._entityService.fetchListEntityCanConnectSubjectProcess(data)
    .subscribe(
      success => {
        this.listEntityCanMoveToSubject = success;
      },
      error => console.log("Error!",error)
    )
  }


  onChange() {
    this.idSubject = this.registrationForm.value.id_subject;
    
    this.listProcess();


  }

  move() {
    let data={id_super:this._auth.getDataLogged().super._key,listIdProcess:this.listIdProcessToMove,id_entityFrom:this.idEntity, id_entityTo:this.registrationForm.value.id_entityTo}
    this._movement.moveListProcessToEntity(data)
    .subscribe(
      success => {
        this.listProcess();
        this.reset()
    },
      error => console.log('error',error)      
    )
  }

  destacarId() {
    this.filterSelection(this.registrationForm.value.id_process)
  }

  filterSelection(query:string) {
    var elements, i;
    elements = document.getElementsByClassName("dinamic-show");
    
    // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
    for (i = 0; i < elements.length; i++) {
      this.removeClass(elements[i], query);
    }
  }
    
  // Hide elements that are not selected
  removeClass(element, query) {

    let index = element.className.indexOf(query);
    
    if(index < 0) {
      element.style.display="none"
    } else {
      element.style.display="block"
    }

  }
  
  // Add active class to the current control button (highlight it)  
}
