import { Phase3Component } from './config/processAdministrativeSubjectProcess/phase3/phase3.component';
import { Phase2Component } from './config/processAdministrativeSubjectProcess/phase2/phase2.component';
import { NewEditRemoveComponent } from './config/administrativeSubjectProcess/new-edit-remove/new-edit-remove.component';
import { ConfigComponent } from './config/config/config.component';
import { CanEntityConnectSubjectProcessBeginEndComponent } from './administrativeSubjectProcess/can-entity-connect-subject-process-begin-end/can-entity-connect-subject-process-begin-end.component';
import { ChoiceAdministrativeUnitComponent } from './administrativeSubjectProcess/choice-administrative-unit/choice-administrative-unit.component';
import { ProcessAdministrativeSubjectProcessComponent } from './config/processAdministrativeSubjectProcess/process-administrative-subject-process/process-administrative-subject-process.component';
import { InitComponent } from './administrativeSubjectProcess/init/init.component';
import { Process } from './class/process';
import { UserLoggedGuard } from './user-logged.guard';
import { UserChangePasswordComponent } from './user-change-password/user-change-password.component';
import { ButtonAlertComponent } from './button-alert/button-alert.component';
import { SearchProcessComponent } from './search-process/search-process.component';
import { TextEditorComponent } from './text-editor/text-editor.component';
import { LogoutComponent } from './logout/logout.component';
import { FirstComponent } from './first/first.component';
import { AuthAdministratorGuard } from './auth-administrator.guard';
import { ListPersonComponent } from './list-person/list-person.component';
import { DisconnectPersonComponent } from './disconnect-person/disconnect-person.component';
import { UserAdmComponent } from './user-adm/user-adm.component';
import { ConnectPersonComponent } from './connect-person/connect-person.component';
import { ListLegalPersonComponent } from './list-legal-person/list-legal-person.component';
import { CanEntityConnectSubjectProcessComponent } from './can-entity-connect-subject-process/can-entity-connect-subject-process.component';
import { HasSubjectProcessSubjectDocumentComponent } from './has-subject-process-subject-document/has-subject-process-subject-document.component';
import { SubjectDocumentComponent } from './subject-document/subject-document.component';
import { SubjectProcessComponent } from './subject-process/subject-process.component';
import { AddressComponent } from './address/address.component';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { SearchProcessByIdComponent } from './search-process-by-id/search-process-by-id.component';
import { MovementOutComponent } from './movement-out/movement-out.component';
import { SendProcessComponent } from './send-process/send-process.component';
import { ListDocumentComponent } from './list-document/list-document.component';
import { UnitInComponent } from './unit-in/unit-in.component';
import { UserInComponent } from './user-in/user-in.component';
import { PersonInComponent } from './person-in/person-in.component';
import { ListProcessComponent } from './list-process/list-process.component';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { OpenProcessEventComponent } from './open-process-event/open-process-event.component';
import { DocumentComponent } from './document/document.component';
import { ProcessComponent } from './process/process.component';
import { EntityComponent } from './entity/entity.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PendingProcessComponent } from './pending-process/pending-process.component';
import { SearchEntityComponent } from './search-entity/search-entity.component';
import { IndividualPersonComponent } from './individual-person/individual-person.component';
import { ListIndividualPersonComponent } from './list-individual-person/list-individual-person.component';
import { LegalPersonComponent } from './legal-person/legal-person.component';
import { AuthProcessOperatorGuard } from './auth-process-operator.guard';
import { AuthConfigOperatorGuard } from './auth-config-operator.guard';
import { PanelControlProcessComponent } from './panel-control-process/panel-control-process.component';
import { GetProcessComponent } from './get-process/get-process.component';
import { ModalConfirmationComponent } from './modal-confirmation/modal-confirmation.component';
import { NewCanConnectAdministrativeUNitSubjectProcessComponent } from './config/processAdministrativeSubjectProcess/new/new-can-connect-administrative-unit-subject-process/new-can-connect-administrative-unit-subject-process.component';
import { ListAdministrativeSubjectProcessComponent } from './config/list-administrative-subject-process/list-administrative-subject-process.component';
import { EditCanConnectAdministrativeSubjectProcessComponent } from './config/processAdministrativeSubjectProcess/edit-can-connect-administrative-subject-process/edit-can-connect-administrative-subject-process.component';
import { HasAdministrativeSubjectProcessSubjectDocumentComponent } from './config/has-administrative-subject-process-subject-document/has-administrative-subject-process-subject-document.component';
import { AuthConfigOperatorExtGuard } from './auth-config-operator-ext.guard';
import { PendingProcessAdministrativeComponent } from './pending-process-administrative/pending-process-administrative.component';


const routes: Routes = [

  { path: '', redirectTo: '/first', pathMatch: 'full' },
  { path: 'process/get', component: GetProcessComponent, canActivate: [AuthProcessOperatorGuard] },
  { path: 'user/adm', component: UserAdmComponent, canActivate: [AuthAdministratorGuard] },
  { path: 'entity/connect/individualPerson', component: ConnectPersonComponent, canActivate: [AuthAdministratorGuard] },
  { path: 'entity/disconnect/individualPerson', component: DisconnectPersonComponent, canActivate: [AuthAdministratorGuard] },
  { path: 'painelControlProcess/:id', component: PanelControlProcessComponent },
  { path: 'entity', component: EntityComponent },
  { path: 'document', component: DocumentComponent },
  { path: 'openProcessEvent', component: OpenProcessEventComponent },
  { path: 'user/new', component: UserInComponent, canActivate: [AuthAdministratorGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  {
    data: { title: 'Processos Sob Minha Responsabilidade' },
    path: 'process/list', component: ListProcessComponent, canActivate: [AuthProcessOperatorGuard]
  },
  { path: 'entity/person/in/new', component: PersonInComponent },
  { path: 'entity/individualPerson', component: IndividualPersonComponent, canActivate: [AuthConfigOperatorGuard] },
  { path: 'entity/individualPerson/:id', component: IndividualPersonComponent },
  { path: 'individualPerson-process/:id', component: IndividualPersonComponent },
  { path: 'entity/administrative/new', component: UnitInComponent, canActivate: [AuthAdministratorGuard] },
  { path: 'entity/legalPerson/:id', component: LegalPersonComponent },
  { path: 'legalPerson-process/:id', component: LegalPersonComponent },
  { path: 'entity/legalPerson', component: LegalPersonComponent, canActivate: [AuthConfigOperatorGuard] },
  { path: 'process/:id/listDocument', component: DocumentComponent, canActivate: [AuthProcessOperatorGuard] },
  { path: 'process/movement', component: SendProcessComponent, canActivate: [AuthProcessOperatorGuard] },
  //  {path:'process/entity/:id_entity/pending',component:PendingProcessComponent,canActivate:[AuthProcessOperatorGuard]},
  { path: 'administrativeSubjectProcess/process/entity/pending', component: PendingProcessAdministrativeComponent },

  { path: 'process/entity/:id_entity/pending', component: PendingProcessComponent },
  { path: 'process/movement/in', component: MovementOutComponent, canActivate: [AuthProcessOperatorGuard] },
  { path: 'searchProcess/id', component: SearchProcessByIdComponent },
  { path: 'searchProcess', component: SearchProcessComponent },
  { path: 'upload', component: UploadFileComponent, canActivate: [AuthProcessOperatorGuard] },
  { path: 'address', component: AddressComponent },
  { path: 'entity/search', component: SearchEntityComponent },
  { path: 'subjectProcess', component: SubjectProcessComponent, canActivate: [AuthConfigOperatorGuard] },
  { path: 'subjectDocument', component: SubjectDocumentComponent, canActivate: [AuthConfigOperatorGuard] },
  { path: 'hasSubjectProcessSubjectDocument', component: HasSubjectProcessSubjectDocumentComponent, canActivate: [AuthConfigOperatorGuard] },
  { path: 'canEntityConnectSubjectProcessComponent', component: CanEntityConnectSubjectProcessComponent, canActivate: [AuthConfigOperatorGuard] },
  { path: 'individualPerson/list', component: ListIndividualPersonComponent },
  { path: 'individualPerson/:id/list', component: ListIndividualPersonComponent },
  { path: 'legalPerson/list', component: ListLegalPersonComponent },
  { path: 'person/list', component: ListPersonComponent, canActivate: [AuthConfigOperatorGuard] },
  { path: 'process-new', component: ListPersonComponent, canActivate: [AuthProcessOperatorGuard] },
  { path: 'legalPerson/:id/list', component: ListLegalPersonComponent },
  { path: 'process/to/entity/:id', component: ProcessComponent, canActivate: [AuthProcessOperatorGuard] },
  { path: 'process/new', component: ListPersonComponent, canActivate: [AuthProcessOperatorGuard] },
  { path: 'person/:id/list', component: ListPersonComponent },
  { path: 'first', component: FirstComponent },
  { path: 'user/password/change', component: UserChangePasswordComponent, canActivate: [UserLoggedGuard] },
  {
    path: 'admininstrativeSubjectProcess',
    component: ProcessAdministrativeSubjectProcessComponent, canActivate: [AuthConfigOperatorExtGuard],
    children: [
      { path: '', redirectTo: 'init', pathMatch: 'full' },
      { path: 'init', component: InitComponent },
      { path: 'choiceAdministrativeUnit', component: ChoiceAdministrativeUnitComponent },
      { path: 'canConnectSubjectProcess', component: CanEntityConnectSubjectProcessBeginEndComponent },
    ]
  },
  {
    path: 'config',
    component: ConfigComponent,
    children: [
      { path: "hasAdministrativeSubjectProcessSubjectDocument", component: HasAdministrativeSubjectProcessSubjectDocumentComponent, canActivate: [AuthConfigOperatorExtGuard] },
      { path: "administrativeSubjectProcess", component: NewEditRemoveComponent, canActivate: [AuthConfigOperatorExtGuard] },
      { path: "listAdministrativeSubjectProcess", component: ListAdministrativeSubjectProcessComponent, canActivate: [AuthConfigOperatorExtGuard] },
      {
        path: 'processAdministrativeSubjectProcess', component: ProcessAdministrativeSubjectProcessComponent, canActivate: [AuthConfigOperatorExtGuard],
        children: [
          { path: 'phase1', component: NewCanConnectAdministrativeUNitSubjectProcessComponent, canActivate: [AuthConfigOperatorExtGuard] },
          { path: 'phase2', component: Phase2Component, canActivate: [AuthConfigOperatorExtGuard] },
          { path: 'phase3', component: Phase3Component, canActivate: [AuthConfigOperatorExtGuard] },
          { path: 'phase4', component: EditCanConnectAdministrativeSubjectProcessComponent, canActivate: [AuthConfigOperatorExtGuard] },

        ]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent = [EntityComponent, ProcessComponent, DocumentComponent, ListProcessComponent, OpenProcessEventComponent, UserComponent]
