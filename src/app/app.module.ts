import { AuthGuard } from './auth.guard';
import { TokenInterceptorService } from './service/token-interceptor.service';
import {LoadingBarHttpClientModule} from '@ngx-loading-bar/http-client'
import { FileSelectDirective } from 'ng2-file-upload';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule, routingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProcessComponent } from './process/process.component';
import { DocumentComponent } from './document/document.component';
import { OpenProcessEventComponent } from './open-process-event/open-process-event.component';
import { PersonComponent } from './person/person.component';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './login/login.component';
import { ListProcessComponent } from './list-process/list-process.component';
import { DashboardProcessComponent } from './dashboard-process/dashboard-process.component';
import { DashboardEntityInComponent } from './dashboard-entity-in/dashboard-entity-in.component';
import { DashboardEntityOutComponent } from './dashboard-entity-out/dashboard-entity-out.component';
import { DashboardSystemComponent } from './dashboard-system/dashboard-system.component';
import { PersonInComponent } from './person-in/person-in.component';
import { UnitInComponent } from './unit-in/unit-in.component';
import { UnitOutComponent } from './unit-out/unit-out.component';
import { DashboardUserComponent } from './dashboard-user/dashboard-user.component';
import { UserInComponent } from './user-in/user-in.component';
import { UserOutComponent } from './user-out/user-out.component';
import { PanelControlProcessComponent } from './panel-control-process/panel-control-process.component';
import { ListDocumentComponent } from './list-document/list-document.component';
import { SendProcessComponent } from './send-process/send-process.component';
import { PendingProcessComponent } from './pending-process/pending-process.component';
import { InformationProcessDespachoComponent } from './information-process-despacho/information-process-despacho.component';
import { MovementOutComponent } from './movement-out/movement-out.component';
import { SearchProcessByIdComponent } from './search-process-by-id/search-process-by-id.component';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { AngularFileUploaderModule } from "angular-file-uploader";
import { ViewerPdfComponent } from './viewer-pdf/viewer-pdf.component';
import { AddressComponent } from './address/address.component';
import { SearchEntityComponent } from './search-entity/search-entity.component';
import { PreProcessComponent } from './pre-process/pre-process.component';
import { HasSubjectProcessSubjectDocumentComponent } from './has-subject-process-subject-document/has-subject-process-subject-document.component';
import { SubjectProcessComponent } from './subject-process/subject-process.component';
import { SubjectDocumentComponent } from './subject-document/subject-document.component';
import { CanEntityConnectSubjectProcessComponent } from './can-entity-connect-subject-process/can-entity-connect-subject-process.component';
import { DashboardConfigComponent } from './dashboard-config/dashboard-config.component';
import { AnnulationDocumentComponent } from './annulation-document/annulation-document.component';
import { IndividualPersonComponent } from './individual-person/individual-person.component';
import { ListIndividualPersonComponent } from './list-individual-person/list-individual-person.component';
import { ControlManagerInvidualPersonComponent } from './control-manager-invidual-person/control-manager-invidual-person.component';
import { LegalPersonComponent } from './legal-person/legal-person.component';
import { ListLegalPersonComponent } from './list-legal-person/list-legal-person.component';
import { ListUnitComponent } from './list-unit/list-unit.component';
import { ConnectPersonComponent } from './connect-person/connect-person.component';
import { DisconnectPersonComponent } from './disconnect-person/disconnect-person.component';
import { UserAdmComponent } from './user-adm/user-adm.component';
import { ListPersonComponent } from './list-person/list-person.component';
import { FirstComponent } from './first/first.component';
import { LogoutComponent } from './logout/logout.component';
import { TextEditorComponent } from './text-editor/text-editor.component';
import { AngularEditorModule } from '@kolkov/angular-editor';

import { GetProcessComponent } from './get-process/get-process.component';
import { SearchProcessComponent } from './search-process/search-process.component';
import { ModalConfirmationComponent } from './modal-confirmation/modal-confirmation.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ButtonAlertComponent } from './button-alert/button-alert.component';
import { UserChangePasswordComponent } from './user-change-password/user-change-password.component';
import { AuthService } from './service/auth.service';
import { InitComponent } from './administrativeSubjectProcess/init/init.component';
import { ProcessAdministrativeSubjectProcessComponent } from './config/processAdministrativeSubjectProcess/process-administrative-subject-process/process-administrative-subject-process.component';
import { ChoiceAdministrativeUnitComponent } from './administrativeSubjectProcess/choice-administrative-unit/choice-administrative-unit.component';
import { CanEntityConnectSubjectProcessBeginEndComponent } from './administrativeSubjectProcess/can-entity-connect-subject-process-begin-end/can-entity-connect-subject-process-begin-end.component';
import { NewEditRemoveComponent } from './config/administrativeSubjectProcess/new-edit-remove/new-edit-remove.component';
import { ConfigComponent } from './config/config/config.component';
import { NewCanConnectAdministrativeUNitSubjectProcessComponent } from './config/processAdministrativeSubjectProcess/new/new-can-connect-administrative-unit-subject-process/new-can-connect-administrative-unit-subject-process.component';
import { Phase2Component } from './config/processAdministrativeSubjectProcess/phase2/phase2.component';
import { Phase3Component } from './config/processAdministrativeSubjectProcess/phase3/phase3.component';
import { ListAdministrativeSubjectProcessComponent } from './config/list-administrative-subject-process/list-administrative-subject-process.component';
import { EditCanConnectAdministrativeSubjectProcessComponent } from './config/processAdministrativeSubjectProcess/edit-can-connect-administrative-subject-process/edit-can-connect-administrative-subject-process.component';
import { HasAdministrativeSubjectProcessSubjectDocumentComponent } from './config/has-administrative-subject-process-subject-document/has-administrative-subject-process-subject-document.component';
import { PendingProcessAdministrativeComponent } from './pending-process-administrative/pending-process-administrative.component';


registerLocaleData(localePt);

@NgModule({
  declarations: [
    ModalConfirmationComponent,
    FileSelectDirective,
    AppComponent,
    routingComponent,
    ProcessComponent,
    DocumentComponent,
    OpenProcessEventComponent,
    PersonComponent,
    UserComponent,
    LoginComponent,
    ListProcessComponent,
    DashboardProcessComponent,
    DashboardEntityInComponent,
    DashboardEntityOutComponent,
    DashboardSystemComponent,
    PersonInComponent,
    UnitInComponent,
    UnitOutComponent,
    DashboardUserComponent,
    UserInComponent,
    UserOutComponent,
    PanelControlProcessComponent,
    ListDocumentComponent,
    SendProcessComponent,
    PendingProcessComponent,
    InformationProcessDespachoComponent,
    MovementOutComponent,
    SearchProcessByIdComponent,
    UploadFileComponent,
    ViewerPdfComponent,
    AddressComponent,
    SearchEntityComponent,
    PreProcessComponent,
    HasSubjectProcessSubjectDocumentComponent,
    SubjectProcessComponent,
    SubjectDocumentComponent,
    CanEntityConnectSubjectProcessComponent,
    DashboardConfigComponent,
    AnnulationDocumentComponent,
    IndividualPersonComponent,
    ListIndividualPersonComponent,
    ControlManagerInvidualPersonComponent,
    LegalPersonComponent,
    ListLegalPersonComponent,
    ListUnitComponent,
    ConnectPersonComponent,
    DisconnectPersonComponent,
    UserAdmComponent,
    ListPersonComponent,
    FirstComponent,
    LogoutComponent,
    TextEditorComponent,
    GetProcessComponent,
    SearchProcessComponent,
    ButtonAlertComponent,
    UserChangePasswordComponent,
    InitComponent,
    ProcessAdministrativeSubjectProcessComponent,
    ChoiceAdministrativeUnitComponent,
    CanEntityConnectSubjectProcessBeginEndComponent,
    NewEditRemoveComponent,
    ConfigComponent,
    NewCanConnectAdministrativeUNitSubjectProcessComponent,
    Phase2Component,
    Phase3Component,
    ListAdministrativeSubjectProcessComponent,
    EditCanConnectAdministrativeSubjectProcessComponent,
    HasAdministrativeSubjectProcessSubjectDocumentComponent,
    PendingProcessAdministrativeComponent,
  ],
  imports: [
    AngularEditorModule,
    LoadingBarHttpClientModule,
    AngularFileUploaderModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
