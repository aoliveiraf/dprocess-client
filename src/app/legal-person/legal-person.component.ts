import { FormBuilder } from '@angular/forms';
import { AuthService } from './../service/auth.service';
import { EntityService } from './../service/entity.service';
import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NewPersonService } from '../service/new-person.service';

@Component({
  selector: 'app-legal-person',
  templateUrl: './legal-person.component.html',
  styles: ['']
})
export class LegalPersonComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, private _serviceEntity: EntityService, private _auth: AuthService, private _fb: FormBuilder, private serviceNewPerson: NewPersonService) { }
  address: any;
  status: string;
  legalperson: boolean = false;
  idExist: boolean = false;

  @Output() public newEntityEnvent = new EventEmitter();
  @Input() public person: any;

  registrationForm = this._fb.group({
    name: [''],
    id: [''],
    id_creator: [''],
    email: [''],
    phone: [''],
    complemento: [''],
    firstId: [''],
  });

  throwToProcess:boolean = false;
  ngOnInit() {

    if (this.route.routeConfig.path.indexOf('process')>=0) {
      this.throwToProcess = true;
    }

    if (!!this.route.snapshot.params.id) {
      this._serviceEntity.fetchEntityById(this.route.snapshot.params.id).
        subscribe(
          (success: any) => {
            this.person = success.pop();

            this.registrationForm.setValue({
              id_creator: this._auth.getDataLogged().entity._key || '',
              name: this.person.name || '',
              id: this.person.id || '',
              email: this.person.email || '',
              phone: this.person.phone || '',
              complemento: this.person.address.complemento || '',
              firstId: this.person.address.firstId || '',
            })
            this.address = this.person.address;
            
            this.status = "editing";
          }
        )
    } else {
      this.status = "creating";
    }
  }

  reciverEventAddress(resposta: any) {

    this.address = resposta;
  }

  changeId() {

    if (this.registrationForm.value.id.length == 14) {
      this._serviceEntity.fetchEntityById(this.registrationForm.value.id).
        subscribe(
          (success: any) => {

            if (success.length > 0) {
              this.idExist = true;
            } else {
              this.idExist = false;
            }
          }
        )
    } else if (this.idExist) {
      this.idExist = false;
    }
  }
  onSubmitToList() {
    let dataLogged = this._auth.getDataLogged();

    let submitEntity = this.registrationForm.value;
    if(dataLogged == null) {
      submitEntity.id_creator = "neutal/0";  
    } else {
      submitEntity.id_creator = dataLogged.entity._key;
    }

    submitEntity.id_address = this.address._key;
    
    if (!!this.person && !!this.person._key) {

      submitEntity._key = this.person._key;

      this._serviceEntity.sendLegalPersonToUpdate(submitEntity)
        .subscribe(
          (success:any) => {
            this.newEntityEnvent.emit(success);
            this.router.navigate([`/person/${this.person.id}/list`]);
          },
          error => console.log("Error!", error)
        )

    } else {

      this.serviceNewPerson.sendLegalPerson(submitEntity)
        .subscribe(
          success => {
            this.newEntityEnvent.emit(success);
            this.router.navigate([`/person/${success.id}/list`]);
          },
          error => console.log("Error!", error)
        )
    }
  }

  onSubmit() {

    if(this.throwToProcess) {

      this.onSubmitToProcess();
    } else {

      this.onSubmitToList();
    }
  }

  onSubmitToProcess() {
    let dataLogged = this._auth.getDataLogged();

    let submitEntity = this.registrationForm.value;
    if(dataLogged == null) {
      submitEntity.id_creator = "neutal/0";  
    } else {
      submitEntity.id_creator = dataLogged.entity._key;
    }

    submitEntity.id_address = this.address._key;
/*
    this.serviceNewPerson.sendLegalPerson(submitEntity)
      .subscribe(
        success => {
          console.log("Success!", success);
          this.newEntityEnvent.emit(success);
          this.router.navigate([`/process/to/entity/${this.person.id}`]);
        },
        error => console.log("Error!", error)
      )
*/
      if (!!this.person && !!this.person._key) {

        submitEntity._key = this.person._key;
  
        this._serviceEntity.sendLegalPersonToUpdate(submitEntity)
          .subscribe(
            (success:any) => {
              this.newEntityEnvent.emit(success);
              this.router.navigate([`/process/to/entity/${this.person.id}`]);
            },
            error => console.log("Error!", error)
          )
  
      } else {
  
        this.serviceNewPerson.sendLegalPerson(submitEntity)
          .subscribe(
            success => {
              this.newEntityEnvent.emit(success);
              this.router.navigate([`/process/to/entity/${this.person.id}`]);
            },
            error => console.log("Error!", error)
          )
      }
  
  }

}
