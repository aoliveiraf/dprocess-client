import { AuthService } from './../service/auth.service';
import { SubjectService } from './../service/subject.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-subject-process',
  templateUrl: './subject-process.component.html',
  styles: ['']
})
export class SubjectProcessComponent implements OnInit {

  constructor(private _auth: AuthService, private _fb: FormBuilder, private _serviceSubjectProcess: SubjectService) { }
  status: string = "creating";
  listing:boolean = false;
  _submit: boolean = false;

  subjectProcessForm = this._fb.group({
    name: [''],
    _key: ['']
  })
  _listSubjectProcess: any;

  ngOnInit() {
    this.listSubjectProcess('');
  }

  listSubjectProcess(partName: string) {
    this._serviceSubjectProcess.listSubjectProcess(partName).subscribe(
      (success: any) => {
        this._listSubjectProcess = success;
        if(success.length >0) {
          this.listing = true;
        } else {
          this.listing = false;
        }
        this.testDisabled(success,partName)
      },
      error => console.log('Error!', error)
    )
  }

  testDisabled(listSubjectProcess:any [],partName:string) {

    const finded = listSubjectProcess.find((element:any)=>{
    
      return element.name.toLowerCase() == partName.toLowerCase();
    }) 
    
    this._submit = !finded && this.subjectProcessForm.valid
  }

  onKeyUpName() {

    this.listSubjectProcess(this.subjectProcessForm.value.name);
  }

  changingName() {

    this.listSubjectProcess(this.subjectProcessForm.value.name);
  }
  delete(subjectProcess: any) {
    this.status='creating';
    this._serviceSubjectProcess.delete(subjectProcess._key).subscribe(
      (success: any) => {
        this.listSubjectProcess(this.subjectProcessForm.value.name);
      },
      error => console.log('Error!', error)
    )
  }

  onEdit(subjectProcess: any) {
    this.status = 'editing';
    this.subjectProcessForm.setValue({
      name: subjectProcess.name,
      _key: subjectProcess._key
    })
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  onNew() {
    this.status = 'creating';
    this.subjectProcessForm.setValue({
      name:'',
      _key:''
    })
  }

  onSubmit() {
    let submitData = this.subjectProcessForm.value;
    submitData.key_super = this._auth.getDataLogged().super._key;
    this._serviceSubjectProcess.saveSubjectProcess(submitData,this.status)
    .subscribe(
      (success: any) => {
        this.onNew();
        this.listSubjectProcess(success.name);
      },
      error => console.log('Error!', error)

    )
  }
}
