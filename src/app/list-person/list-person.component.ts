import { EntityService } from './../service/entity.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-person',
  templateUrl: './list-person.component.html',
  styles: ['']
})
export class ListPersonComponent implements OnInit {

  constructor(private _fb: FormBuilder, private router:Router, private route: ActivatedRoute, private serviceEntity: EntityService) { }
  showButtonToProcess=false;
  _listEntity: any;
  sForm = this._fb.group(
    {
      id: [''],
      name: ['']
    }
  );

  throwToProcess:boolean = false;

  ngOnInit() {

    if (this.route.routeConfig.path.indexOf('process')>=0) {
      this.throwToProcess = true;
    }

    if (!!this.route.snapshot.params.id) {
      
      this.loadForm({ id: this.route.snapshot.params.id,name:''});
      this.onSubmit();
    }
  }

  loadForm(data: any) {
    this.sForm.setValue({
      id: data.id,
      name: data.name
    })
  }

  onThrowToProcess(entity:any) {

    this.router.navigate([`/process/to/entity/${entity.id}`])
  }

  onEdit(entity:any) {
    
    if(entity._type=='LegalPerson') {
      if(this.throwToProcess) {

        this.router.navigate([`/legalPerson-process/${entity.id}`])
      } else {

        this.router.navigate([`/entity/legalPerson/${entity.id}`])
      }
    } if(entity._type=='IndividualPerson') {
      if(this.throwToProcess) {
        this.router.navigate([`/individualPerson-process/${entity.id}`])
      } else {
        this.router.navigate([`/entity/individualPerson/${entity.id}`])
      }
    }
  }

  onSubmit() {

    this.serviceEntity.fetchEntityById(this.sForm.value.id).
      subscribe(
        (success: any) => {
          this._listEntity = success;
        },
        error => {
          console.log('error', error)
        }
      )
  }

}
