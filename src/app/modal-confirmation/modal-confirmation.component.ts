import { Component, OnInit, Input, ContentChild, TemplateRef, ElementRef } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';


@Component({
  selector: 'app-modal-confirmation',
  templateUrl: './modal-confirmation.component.html',
  styles: [''],
  providers: [NgbModalConfig, NgbModal]
})
export class ModalConfirmationComponent implements OnInit {

  _message:any;
  _textButton:any;
  _nameRoute:any;

  @Input() public set nameRoute(nameRoute:any){
    this._nameRoute=nameRoute;
  }
  public get nameRoute():any{
    return this._nameRoute;
  }

  @Input() public set textButton(textButton:any){
    this._textButton=textButton;
  }
  public get textButton():any{
    return this._textButton;
  }

  @Input() public set message(message:any){
    this._message=message;
  }
  public get message():any{
    return this._message;
  }
  @ContentChild('content') content: TemplateRef<ElementRef>;

  constructor(config: NgbModalConfig, private modalService: NgbModal,  private route:Router) {
    // customize default values of modals used by this component tree
    config.backdrop = 'static';
    config.keyboard = false;
  }

  redirect() {
    
    this.route.navigate([this.nameRoute])
    

  }

  open(content) {
    this.modalService.open(content);
  }
  ngOnInit() {
    

  }
  ngAfterViewInit(){
    console.log('ngAfterViewInit');
    
    this.modalService.open(this.content);
  }

}
