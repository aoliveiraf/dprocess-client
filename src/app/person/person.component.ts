import { ListEntityService } from './../service/list-entity.service';
import { FormBuilder } from '@angular/forms';
import { NewPersonService } from './../service/new-person.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  listEntity:any = [];
  currentEntity:any;
  constructor(private _fb:FormBuilder,private serviceListEntity:ListEntityService, private serviceNewPerson:NewPersonService) { }

  registrationForm = this._fb.group({
    name: [''],
    id: [''],
    id_creator: [''],
    id_super: ['']
  });
  
  ngOnInit() {
  }

  onKeyUp() {
    this.serviceListEntity.listAdministrative(this.registrationForm.value)
    .subscribe(
       success=> this.listEntity=success,
      error=> console.log("error!",error)
    )
    
  }

  onEntityChange() {
    this.currentEntity = this.getSelecteEntitytByName(this.registrationForm.value.id_entity);
    this.updateForm();
  }

  updateForm() {
    this.registrationForm.setValue({
      name: this.registrationForm.value.name,
      id: this.registrationForm.value.id,
      id_creator: this.registrationForm.value.id_creator,
      id_super: this.registrationForm.value.id_super
    });

  }

  getSelecteEntitytByName(selectedKey: string): any {
    return this.listEntity.find(entity => entity._key === selectedKey);
  }

  onSubmit() {
    let submitEntity = this.registrationForm.value;
    submitEntity.id_entity = this.currentEntity._key;

    this.serviceNewPerson.sendIndividualPersonIn(submitEntity)
    .subscribe(
      success => console.log("Success!",success),
      error => console.log("Error!", error)
    )
  }
}
