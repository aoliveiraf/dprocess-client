import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-control-manager-invidual-person',
  templateUrl: './control-manager-invidual-person.component.html',
  styles: ['']
})
export class ControlManagerInvidualPersonComponent implements OnInit {

  constructor(private activatedRoute:ActivatedRoute) { 
  }
  showListForm = true;
  showIndividualPersonForm = false; 

  ngOnInit() {
    let path =this.activatedRoute.snapshot.routeConfig.path;
    if(path.match('list')) {
      this.showListForm = true;
      this.showIndividualPersonForm = false;
    } else {
      this.showIndividualPersonForm = true;
      this.showListForm = false;
    }
  }

}
