import { AuthService } from './service/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthConfigOperatorGuard implements CanActivate {

  constructor(private _authService: AuthService, private _router: Router) { }

  canActivate(): boolean {
    if (!!this._authService.getDataLogged()) {
      let user = this._authService.getDataLogged().user;
      if (user.listType.indexOf('ConfigOperator') >= 0) {
        return true;
      } else {
        return false;
      }
    } else {
      this._router.navigate(['/login']);
      return false;
    }
  }
}
