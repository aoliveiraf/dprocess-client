import { AuthService } from './../service/auth.service';
import { EntityService } from './../service/entity.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MovementProcessService } from '../service/movement-process.service';

@Component({
  selector: 'app-movement-out',
  templateUrl: './movement-out.component.html',
  styles: ['']
})
export class MovementOutComponent implements OnInit {
  _listProcess:any;
  listSubject:any;
  idEntity:string;
  idSubject:string;
  listEntityPersonIn:any;
  isShowDispatch:boolean = false;
  listIdProcessToMove = [];
  constructor(private _router:Router, private _fb:FormBuilder, private _entityService:EntityService, private _movement:MovementProcessService,private _auth:AuthService) { }

  registrationForm =  this._fb.group({
    id_subject:[''],
    id_entityTo:['']
  })
  ngOnInit() {
   this.idEntity = this._auth.getDataLogged().entity._key;
    this.lookupSubject(this.idEntity);

  }

  lookupSubject(idEntity:string) {

    this._entityService.listSubjectFromProcess(idEntity).
    subscribe(
      success => this.listSubject = success,
      error => console.log("Error!", error)
    )
  }

  dispatch() {
    this.isShowDispatch = !this.isShowDispatch;
  }
  listProcess() {
    let data = {id_subject:this.idSubject,id_entity:this.idEntity}
    this._entityService.listProcess(data)
    .subscribe(
      success => {
        this._listProcess = success;
        this.fetchListEntityPersonIn();
      },
      error => console.log("Error!",error)
    )
  }

  onChangeCheck(id_process) {
    let index = this.listIdProcessToMove.indexOf(id_process);
    if(index==-1) {
      this.listIdProcessToMove.push(id_process);
    } else {
      this.listIdProcessToMove.splice(index,1)
    }
  }

  fetchListEntityPersonIn() {
    let data = {id_entity:this._auth.getDataLogged().entity._key,id_super:this._auth.getDataLogged().super._key}
    this._entityService.fetchListEntityPersonIn(data)
    .subscribe(
      success => {
        this.listEntityPersonIn = success;
      },
      error => console.log("Error!",error)
    )
  }


  onChange() {
    this.idSubject = this.registrationForm.value.id_subject;
    this.listProcess();

  }

  move() {
    let data={id_super:this._auth.getDataLogged().super._key,listIdProcess:this.listIdProcessToMove,id_entityFrom:this.idEntity, id_entityTo:this.registrationForm.value.id_entityTo}
    this._movement.moveListProcessToEntity(data)
    .subscribe(
      success => {
        this.listProcess();
    },
      error => console.log('error',error)      
    )
  }

}
