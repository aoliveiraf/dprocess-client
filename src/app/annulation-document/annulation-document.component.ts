import { AuthService } from './../service/auth.service';
import { NewDocumentService } from './../service/new-document.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-annulation-document',
  templateUrl: './annulation-document.component.html',
  styles: ['']
})
export class AnnulationDocumentComponent implements OnInit {

  constructor(private _fb:FormBuilder,private _documentService:NewDocumentService, private _auth:AuthService) { }
  registrationForm = this._fb.group({
    justification:['']
  })
  _indexComponent:string;
  @Input() public documentDetail:any;
  @Input() set indexComponent(index:any){
    this._indexComponent="component"+index;
  }

  @Output() public submitEvent = new EventEmitter();


  ngOnInit() {
  }

  showORhide() {
    var x = document.getElementById(this._indexComponent);
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  onSubmit() {
    let submitData = {
      key_super:this._auth.getDataLogged().super._key,
      key_creator:this._auth.getDataLogged().entity._key,
      key_document:this.documentDetail.document._key,
      justification:this.registrationForm.value.justification
    }
    this._documentService.submitAnnulation(submitData).
    subscribe(
      success => {
        let data:any = success;
        this.submitEvent.emit(data);
      },
      error => console.log('error!',error)
      
    )

    }
}
