import { AuthService } from './../service/auth.service';
import { EntityService } from './../service/entity.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-connect-person',
  templateUrl: './connect-person.component.html',
  styles: ['']
})
export class ConnectPersonComponent implements OnInit {

  constructor(private _fb: FormBuilder, private _auth: AuthService, private router: Router, private route: ActivatedRoute, private serviceEntity: EntityService) { }

  listEntity: any;
  listUnit: any;
  _listSubEntity: any;
  sForm = this._fb.group(
    {
      id: ['']
    }
  );

  registrationForm = this._fb.group(
    {
      key_super: [''],
      key_entity: [''],
      registrationId:['']
    }
  )

  ngOnInit() {

    this.listSubEntityAdm('')
  }

  onChangeEntity() {

    this.listSubEntity(this.registrationForm.value.key_super);
  }

  listSubEntity(key_entity: string) {

    this.serviceEntity.listSubEntity(key_entity, '').
      subscribe(
        (success2: any) => {
          this._listSubEntity = success2;
        },
        error1 => console.log('Error', error1)
      )
  }

  listSubEntityAdm(partName: string) {

    this._auth.lookupEntityAdministared(this._auth.getDataLogged().user._key).
      subscribe(
        (success1: any) => {
          let entity = success1.pop();

          this.serviceEntity.listSubAdministrativeEntity(entity._key, partName).
            subscribe(
              (success2: any) => {
                this.listUnit = success2;
              },
              error1 => console.log('Error', error1)
            )
        },
        error2 => {
          error2 => console.log('Error', error2)
        }
      )

  }

  onSubmit() {
    this.serviceEntity.fetchEntityById(this.sForm.value.id).
      subscribe(
        success => {
          this.listEntity = success;
        },
        error => {
          console.log('error', error)
        }
      )
  }

  loadForm(data: any) {
    this.sForm.setValue({
      id: data.id,
      name: data.name
    })
  }

  connect(entity: any) {
    
    let data = this.registrationForm.value;
    data.key_entity = entity._key;
    data.key_creator = this._auth.getDataLogged().entity._key;

    this.serviceEntity.connect(data).subscribe(
      success => {
        this.listSubEntity(data.key_super);
      },
      error => {
        console.log('error',error);
        
      }
    )
  }

  onEdit(entity: any) {

    if (entity.id.length == 11) {
      this.router.navigate([`/entity/individualPerson/${entity.id}`])
    }
  }

  lookypAddress() {

    /*
    this.serviceEntity.fetchEntityById(this.sForm.value.id).
    subscribe(
      success => {
        this.listEntity = success;
      },
      error => {        
        console.log('error',error)
      }
    )
*/
  }
}

