import { ListDocumentComponent } from './../list-document/list-document.component';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { ListProcessService } from './../service/list-process.service';
import { AuthService } from './../service/auth.service';
import { EntityService } from './../service/entity.service';
import { FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { NewDocumentService } from '../service/new-document.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styles: ['']
})
export class DocumentComponent implements OnInit {
  @Input() idProcess: string;

  constructor(config: NgbModalConfig, private modalService: NgbModal,private _router:Router,private _serviceEntity: EntityService, private _auth: AuthService, private _fb: FormBuilder, private _route: ActivatedRoute, private _serviceProcess: ListProcessService, private _serviceDocument: NewDocumentService) {
    config.backdrop = 'static';
    config.keyboard = false;

   }

  showText: boolean = false;
  currentProcess: any;
  listDocument: any;
  listSubjectDocumentDetail: any;
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '5',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',
  };


  registrationForm = this._fb.group(
    {
      id_subject: [''],
      text: ['']
    })

  ngOnInit() {
    console.log('oninit');
    
    this._serviceProcess.detail(this.idProcess)
      .subscribe(
        (success: any) => { this.currentProcess = success.process },
        error => console.log("Error!", error)
      )
    this.lookupListDocument();
    this.lookupListSubjectDocument();
  }

  showORhide(id) {
    var x = document.getElementById(id);

    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  reset() {

    this.registrationForm.setValue(
      {
        id_subject:'',
        text:''
      })
  
  }

  onChange() {
    let element = this.findLocalSubjectDocumentDetail(this.registrationForm.value.id_subject);
    if (element._type == 'Ordinary') {
      this.showText = false;
    } else {
      this.showText = true;
    }

    this.updateRegistrationForm()
  }
  findLocalSubjectDocumentDetail(selectKey: string): any {
    return this.listSubjectDocumentDetail.find(element => {
      if (element._key === selectKey) {
        return element;
      }
    });
  }
  updateRegistrationForm() {
    this.registrationForm.setValue({
      id_subject: this.registrationForm.value.id_subject,
      text: ''
    });
  }

  lookupListSubjectDocument() {
    this._serviceEntity.listSubjectDocument(this._auth.getDataLogged().entity._key, this.idProcess)
      .subscribe(
        (success: any) => {
          this.listSubjectDocumentDetail = success || [];

        },
        error => console.log('Error', error)
      )

  }

  lookupListDocument() {
    
    this._serviceProcess.listDocument(this.idProcess)
      .subscribe(
        success => {

          this.listDocument = success || [];

        },
        error => console.log('Error', error)
      )
  }
  annulationEvent() {
    this.lookupListSubjectDocument();
  }
  onSubmit(modalViewer) {

  
    let dataLogged = this._auth.getDataLogged();

    let submitObject = this.registrationForm.value;
    delete submitObject.showText;
    submitObject.id_process = this.currentProcess._key;
    submitObject.id_creator = dataLogged.entity._key;
    submitObject.id_super = dataLogged.super._key;

    this._serviceDocument.send(submitObject)
      .subscribe(
        success => {
          this.lookupListDocument();
          this.lookupListSubjectDocument();
          this.openModal(modalViewer);
          this.reset();
        },
        error => console.log("Error", error)
      )
  }

  openModal(modalViewer) {
    this._serviceProcess.getProcess(this.idProcess)
    .subscribe(
      (success:any) => {
        if(!success.active) {
          this.modalService.open(modalViewer);
        }
      },
      error => console.log("Error", error)
    )

  }

  redirect() {

    this._router.navigate(['/process/list'])
  }

}
