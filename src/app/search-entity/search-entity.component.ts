import { FormBuilder } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { EntityService } from '../service/entity.service';

@Component({
  selector: 'app-search-entity',
  templateUrl: './search-entity.component.html',
  styles: ['']
})
export class SearchEntityComponent implements OnInit {
  entity:any;
  @Output() entityEvent = new EventEmitter();
  showPerson:boolean;
  constructor(private _fb:FormBuilder,private serviceEntity:EntityService) { }

  entityForm = this._fb.group({
    id:['']
  })

  ngOnInit() {
    this.showPerson = false;
  }

  onSubmit() {
    this.serviceEntity.fetchEntityById(this.entityForm.value.id).
    subscribe(
      success => {
        this.entity = success;
        this.showPerson = true;
        this.entityEvent.emit(this.entity);
      },
      error => {
        
        this.entityEvent.emit(error);
        console.log('error',error)
      }
    )
  }

}
