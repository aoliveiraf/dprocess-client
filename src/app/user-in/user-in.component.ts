import { EntityService } from './../service/entity.service';
import { UserComponent } from './../user/user.component';
import { Entity } from './../class/entity';
import { AuthService } from './../service/auth.service';
import { ListEntityService } from './../service/list-entity.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-in',
  templateUrl: './user-in.component.html',
  styles: [`.switch {
    position: relative;
    display: inline-block;
    width: 60px;
    height: 34px;
  }
  
  .switch input { 
    opacity: 0;
    width: 0;
    height: 0;
  }
  
  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: #ccc;
    -webkit-transition: .4s;
    transition: .4s;
  }
  
  .slider:before {
    position: absolute;
    content: "";
    height: 26px;
    width: 26px;
    left: 4px;
    bottom: 4px;
    background-color: white;
    -webkit-transition: .4s;
    transition: .4s;
  }
  
  input:checked + .slider {
    background-color: #2196F3;
  }
  
  input:focus + .slider {
    box-shadow: 0 0 1px #2196F3;
  }
  
  input:checked + .slider:before {
    -webkit-transform: translateX(26px);
    -ms-transform: translateX(26px);
    transform: translateX(26px);
  }
  
  /* Rounded sliders */
  .slider.round {
    border-radius: 34px;
  }
  
  .slider.round:before {
    border-radius: 50%;
  }
  `
]
})
export class UserInComponent implements OnInit {


  constructor(private _fb: FormBuilder, private serviceEntity: EntityService, private _auth: AuthService) { }

  listEntityUser: any=[];
  listUnit: any;
  editingUser: any;
  isCloneName:boolean=false;
  idRefDiv:string = '';
  buttonTarget:any='';
  noConnectPerson:boolean=false;
//  entityUser:any;

  registrationForm = this._fb.group({
    key_user: [''],
    name: [''],
    key_unit: [''],
    key_entity: [''],
    isAdministrator: false,
    isProcessOperator: false,
    isConfigOperator: false,
    isConfigOperatorExt:false
  });

  ngOnInit() {

    this.listSubEntity('')
  }



  changeName() {

    this._auth.lookupUserByPartName(this.registrationForm.value.name).
    subscribe(
      (success:any) => {
        this.isCloneName = success.length > 0;
      },
      error => console.log('error',error)
      
    )
  }
  newUser(entity:any,event:any) {
    event.target.disabled = true;
    if(this.buttonTarget!='') {
      this.buttonTarget.disabled = false;
    }
    this.buttonTarget = event.target;
    this.isCloneName = false;
    this.registrationForm.setValue({
      key_user: '',
      name: '',
      key_unit: this.registrationForm.value.key_unit,
      key_entity: entity._key,
      isAdministrator: false,
      isConfigOperator: false,
      isConfigOperatorExt:false,
      isProcessOperator: false
    })
    this.showORhide(this.idRefDiv);
    this.idRefDiv = entity._key;
    this.showORhide(this.idRefDiv);
  }
  showORhide(idComponent: any) {
    if(idComponent == '') return; 

    var x = document.getElementById(idComponent);
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  submitUpdateUserAdmin(submitUser: any) {

    this._auth.lookupEntityAdministared(this._auth.getDataLogged().user._key).
      subscribe(
        (success: any) => {
          let entity = success.pop();
          submitUser.key_administared = entity._key;

          this._auth.submitUpdateUserAdmin(submitUser).subscribe(
            success => {
              this.listUser();
              this.resetForm()

            },
            error => console.log('error', error)
          )
        },
        error => {
          error => console.log('Error', error)
        }
      )
  }

  submitNewUserAdmin(submitUser: any) {
    this._auth.lookupEntityAdministared(this._auth.getDataLogged().user._key).
      subscribe(
        (success: any) => {
          let entity = success.pop();
          submitUser.key_administared = entity._key;

          this._auth.submitNewUserAdmin(submitUser).subscribe(
            success => {
              this.listUser();
              this.resetForm()

            },
            error => console.log('error', error)
          )
        },
        error => {
          error => console.log('Error', error)
        }
      )


  }

  submitNewUser(submitUser: any) {

    this._auth.submitNewUser(submitUser).subscribe(
      success => {
        this.listUser();
        this.resetForm()

      },
      error => console.log('error', error)
    )

  }

  submitUpdateUser(submitUser: any) {

    this._auth.submitUpdateUser(submitUser).subscribe(
      success => {
        this.listUser();
        this.resetForm();
      },
      error => console.log('error', error)
    )

  }

  updateUserDeleteAdm(submitUser: any) {

    this._auth.deleteAdm(submitUser).subscribe(
      success => {
        this.submitUpdateUser(submitUser);        
      },
      error => console.log('error',error)
    )

  }

  cancelEdit() {
    this.buttonTarget.disabled = false; 
    this.buttonTarget='';
    this.isCloneName = false;

    this.showORhide(this.registrationForm.value.key_user);
    this.resetForm();
  }

  cancelEditNew(event:any) {
    this.buttonTarget.disabled = false;
    this.buttonTarget = '';
    this.isCloneName = false;
    this.showORhide(this.registrationForm.value.key_entity);
    this.resetForm();
  }

  listSubEntity(partName: string) {

    this._auth.lookupEntityAdministared(this._auth.getDataLogged().user._key).
      subscribe(
        (success1: any) => {
          let entity = success1.pop();

          this.serviceEntity.listSubAdministrativeEntity(entity._key, partName).
            subscribe(
              (success2: any) => {
                this.listUnit = success2;
              },
              error1 => console.log('Error', error1)
            )
        },
        error2 => {
          error2 => console.log('Error', error2)
        }
      )

  }

  onChangeUnit() {
    this.editingUser = undefined;
    this.listUser();
  }

  passwordReset() {

    const key_creator = this._auth.getDataLogged().entity._key;

    this._auth.resetPassword(key_creator,this.editingUser.user._key).subscribe(
      success => {
        console.log(success);
      },
      error => console.log('error', error)
    )

  }
  onChangePerson() {

    let key_entity = this.registrationForm.value.key_entity;
    
    this.editingUser = this.listEntityUser.find(element => {
      
      return element.entity._key==key_entity;
      
    });

    this.registrationForm.setValue({
      key_user: this.editingUser.user._key || '',
      name: this.editingUser.user.name || '',
      key_unit: this.registrationForm.value.key_unit,
      key_entity: this.editingUser.entity._key,
      isAdministrator: this.editingUser.user.listType.indexOf('Administrator') >= 0,
      isConfigOperator: this.editingUser.user.listType.indexOf('ConfigOperator') >= 0,
      isConfigOperatorExt: this.editingUser.user.listType.indexOf('ConfigOperatorExt') >= 0,
      isProcessOperator: this.editingUser.user.listType.indexOf('ProcessOperator') >= 0
    })


  }

  listUser() {

    let key_unit = this.registrationForm.value.key_unit;
    console.log('key_unit',key_unit);
    
    this.serviceEntity.listSubEntityUser(key_unit).subscribe(
      success => {
        this.listEntityUser = success
        if(this.listEntityUser.length==0) {
          this.noConnectPerson = true; 
        } else {
          this.noConnectPerson = false;
        }
        this.editingUser = undefined;
      },
      error => console.log('error', error)
    )
  }

  resetForm() {

    this.registrationForm.setValue({
      key_user: '',
      name: '',
      key_unit: this.registrationForm.value.key_unit,
      key_entity: '',
      isAdministrator: false,
      isProcessOperator: false,
      isConfigOperator: false,
      isConfigOperatorExt: false,

    })
  }


  onSubmit() {
    let submitUser = this.registrationForm.value;
    submitUser.key_creator = this._auth.getDataLogged().entity._key;
    submitUser.listType = [];

    if (submitUser.isProcessOperator) {
      submitUser.listType.push('ProcessOperator');
    }
    if (submitUser.isConfigOperator) {
      submitUser.listType.push('ConfigOperator');
    }
    if (submitUser.isConfigOperatorExt) {
      submitUser.listType.push('ConfigOperatorExt');
    }
    if (submitUser.isAdministrator) {
      submitUser.listType.push('Administrator');
    }
    delete submitUser.isConfigOperator;
    delete submitUser.isConfigOperatorExt;
    delete submitUser.isProcessOperator;
    delete submitUser.isAdministrator;
//    delete submitUser.key_unit;
    if (!!submitUser.key_user) {
      delete submitUser.name;

      if (this.editingUser.user.listType.indexOf('Administrator') >= 0 && submitUser.listType.indexOf('Administrator') < 0) {

        this.updateUserDeleteAdm(submitUser);

      } else if (this.editingUser.user.listType.indexOf('Administrator') < 0 && submitUser.listType.indexOf('Administrator') >= 0) {

        this.submitUpdateUserAdmin(submitUser);

      } else  {

        this.submitUpdateUser(submitUser);
      } 
    } else {

      if (submitUser.listType.indexOf('Administrator') >= 0) {

        this.submitNewUserAdmin(submitUser);
      } else {

        this.submitNewUser(submitUser);
      }
    }

  }

}
