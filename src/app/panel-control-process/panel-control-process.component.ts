import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ListProcessService } from '../service/list-process.service';


@Component({
  selector: 'app-panel-control-process',
  templateUrl: './panel-control-process.component.html',
  styles: ['']
})
export class PanelControlProcessComponent implements OnInit {

  processDetail:any;
  emailAddress:any;
  sendEmailTrue:Boolean=false;
  clickSendProtocol:Boolean=false;

  constructor(private route:ActivatedRoute,private servicePanel:ListProcessService) { }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get('id');

    this.servicePanel.detail(id)
    .subscribe(
      success => {
        this.processDetail = success;
        (this.processDetail);
        
      },
      error =>  console.log('Error!',error)
    )
  }

  sendEmail() {
    this.clickSendProtocol=true;
    this.sendEmailTrue=false;
    this.servicePanel.sendEmail(this.processDetail.process.id)
    .subscribe(
      (success:any) => {
        this.emailAddress=success.concerned.email;
        this.sendEmailTrue=true;
      },
      error =>  {
        console.log('Error!',error);
        this.clickSendProtocol=false;
        this.sendEmailTrue=false;
      }
    )
    

  }
  showORhide() {
    var x = document.getElementById("addDocument");
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

}
