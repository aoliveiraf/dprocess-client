import { AddressService } from './../service/address.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { _iterableDiffersFactory } from '@angular/core/src/application_module';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styles: ['']
})
export class AddressComponent implements OnInit {

  constructor(private _fb: FormBuilder, private _serviceAddress: AddressService,private _auth:AuthService) { }
  addressForm = this._fb.group({
    id: [''],
    logradouro: [''],
    bairro:[''],
    localidade: ['Mossoró'],
    uf: ['RN']
  });
  _address: any;
  mode="search";
  action="none";
  @Input()
  set address(address: any) {
    this._address = address;
    if (!!address) {
      this.addressForm.setValue({
        id: this.address.cep || '',
        logradouro: '',
        bairro:'',
        localidade: 'Mossoró',
        uf: 'RN'
    
      })
    }
  }

  get address(): any { return this._address; }

  @Output() eventAddress = new EventEmitter()

  ngOnInit() {

  }

  onNew() {
    this.mode="new"

    this.addressForm.setValue({
      id: this.addressForm.value.id || '',
      logradouro: '',
      bairro: '',
      uf: 'RN',
      localidade: 'Mossoró'
    })

  }

  onSave() {
    let data = this.addressForm.value;
    data.id_creator = this._auth.getDataLogged().entity._key;

    this._serviceAddress.saveAddress(data).
      subscribe(
        success => {

          this.address = success;
          this.mode="search";
          this.action="none";
          this.eventAddress.emit(this.address);
        },
        (error) => {
          this.action="failed"
          console.log('Error!', error)
        }
      )

  }
  onSubmit() {

    this._serviceAddress.getAddress(this.addressForm.value.id).
      subscribe(
        success => {

          this.address = success;
          this.action="none"          
          this.eventAddress.emit(this.address);
        },
        (error) => {
          this.action="failed"
          console.log('Error!', error)
        }
      )
  }

}
