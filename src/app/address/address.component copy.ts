import { AddressService } from './../service/address.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { _iterableDiffersFactory } from '@angular/core/src/application_module';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styles: ['']
})
export class AddressComponent implements OnInit {

  constructor(private _fb: FormBuilder, private _serviceAddress: AddressService) { }
  addressForm = this._fb.group({
    id: ['']
  });
  _address: any;
  @Input()
  set address(address: any) {
    this._address = address;
    if (!!address) {
      this.addressForm.setValue({
        id: this.address.cep || ''
      })
    }
  }

  get address(): any { return this._address; }

  @Output() eventAddress = new EventEmitter()

  ngOnInit() {

  }

  onSubmit() {

    this._serviceAddress.getAddress(this.addressForm.value.id).
      subscribe(
        success => {

          this.address = success;
          this.eventAddress.emit(this.address);
        },
        error => console.log('Error!', error)
      )
  }

}
