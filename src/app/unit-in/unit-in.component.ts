import { AuthService } from './../service/auth.service';
import { ListEntityService } from './../service/list-entity.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NewPersonService } from '../service/new-person.service';
import { EntityService } from '../service/entity.service';

@Component({
  selector: 'app-unit-in',
  templateUrl: './unit-in.component.html',
  styles: ['']
})
export class UnitInComponent implements OnInit {

  listEntity: any = [];
  currentEntity: any;
  status: string = "creating";
  listing: boolean = false;
  _submit: boolean = false;
  errorMessageDelete = '';
  canDeleteError: boolean = true;

  constructor(private serviceEntity: EntityService, private _auth: AuthService, private _fb: FormBuilder, private serviceListEntity: ListEntityService, private serviceNewPerson: NewPersonService) { }

  registrationForm = this._fb.group({
    name: [''],
    id: [''],
    _key: ['']
  });

  ngOnInit() {
    this.errorMessageDelete = '';
    this.currentEntity = this._auth.getDataLogged().super;
    this.listSubEntity('');
  }

  listSubEntity(partName: string) {

    this._auth.lookupEntityAdministared(this._auth.getDataLogged().user._key).
    subscribe(
      (success1:any) => {
        let entity = success1.pop();
        this.serviceEntity.listSubAdministrativeEntity(entity._key,partName).
        subscribe(
          (success2: any) => {
            this.listEntity = success2;
            if (success2.length > 0) {
              this.listing = true;
            } else {
              this.listing = false;
            }
            this.testDisabled(success2.length)
            this.canDeleteError = false;
          },
          error1 => console.log('Error', error1)
        )  
      },
      error2 => {
        error2 => console.log('Error', error2)
      }
    )

  }
  testDisabled(lengthList: number) {

    if (lengthList > 0) {
      this._submit = false;
    } else if (this.registrationForm.valid) {
      this._submit = true;
    }
  }

  onchangeName() {

    this.listSubEntity(this.registrationForm.value.name);
  }

  onkeyupName() {

    this.listSubEntity(this.registrationForm.value.name);
  }

  getSelecteEntitytByName(selectedKey: string): any {
    return this.listEntity.find(entity => entity._key === selectedKey);
  }

  onNew() {
    this.canDeleteError = false;
    this.status = 'creating';
    this.registrationForm.setValue({
      name: '',
      id: '',
      _key: ''
    })
  }

  onEdit(unit: any) {
    this.status = 'editing';
    this.registrationForm.setValue({
      name: unit.name,
      id: unit.id,
      _key: unit._key
    })
    this.canDeleteError = false;
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  onSubmit() {
    let dataLogged = this._auth.getDataLogged();

    let submitEntity = this.registrationForm.value;
    submitEntity.id_creator = dataLogged.entity._key;
    if (this.status != 'editing') {
      delete submitEntity._key;
      this._auth.lookupEntityAdministared(dataLogged.user._key).
        subscribe(
          (success1: any) => {
            submitEntity.id_super = success1.pop()._key;
            this.serviceNewPerson.sendUnitIn(submitEntity)
              .subscribe(
                (success2: any) => {
                  this.onNew();
                  this.listSubEntity(success2.pop().name);
                },
                error => console.log("Error!", error)
              )
          }
        );
    } else {

      this.serviceEntity.updateAdministrativeUnit(submitEntity)
        .subscribe(
          (success: any) => {
            this.onNew();
            this.listSubEntity(success.pop().name);
          },
          error => console.log("Error!", error)
        )

    }
  }

  delete(entity: any) {

    this.serviceEntity.canDelete(entity._key).subscribe(
      (success: any) => {
        let data = success.pop();
        let _canDelete = (typeof data.listProcess !== undefined && data.listProcess.length == 0) &&
          (typeof data.listSubEntity !== undefined && data.listSubEntity.length == 0) &&
          (typeof data.listPromiseProcess !== undefined && data.listPromiseProcess.length == 0)

        if (_canDelete) {
          this.status = 'creating';
          this.serviceEntity.delete(entity._key).subscribe(
            (success: any) => {
              this.listSubEntity(this.registrationForm.value.name);
            },
            error => console.log('Error!', error)
          )
        } else {
          this.canDeleteError = true;
        }
      },
      error => console.log('error', error)
    )
  }

}
