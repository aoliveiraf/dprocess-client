import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EntityService {

  _url = `${environment.apiUrl}/entity/administrative?searchName=`;

  constructor(private _http:HttpClient, private _auth:AuthService) { }

  listAdministrativeUnit(data:any) {
    return this._http.get(this._url+data.id_entity)
  }
  listSubject(idEntity:string) {
    let url = `${environment.apiUrl}/entity/${idEntity}/listSubject`;
    return this._http.get(url); 
  }

  connect(data:any) {

    let url = `${environment.apiUrl}/entity/connect`;

    return this._http.post(url,data); 
  }

  disconnect(data:any) {

    let url = `${environment.apiUrl}/entity/disconnect`;

    return this._http.post(url,data); 
  }

  delete(key_entity:string) {
    let url = `${environment.apiUrl}/entity/${key_entity}`;

    return this._http.delete(url);
  }
  canDelete(keyEntity:string) {

    let url = `${environment.apiUrl}/entity/${keyEntity}/canDelete`;

    return this._http.get(url);

  }
  updateAdministrativeUnit(data:any) {
    let url = `${environment.apiUrl}/entity/administrative`;

    return this._http.put(url,data);
  }
  getSuper(idEntity:string) {

    let url = `${environment.apiUrl}/entity/${idEntity}/super`;
    return this._http.get(url); 
  }

  listSubjectFromProcess(idEntity:string) {
    let url = `${environment.apiUrl}/entity/${idEntity}/from/process/to/listSubject`;
    return this._http.get(url); 
  }

  listProcessFromEntity(data:any) {
    let url = `${environment.apiUrl}/entity/${data.id_entity}/listProcess`;
    return this._http.get(url); 
  }

  listProcess(data:any) {
    let url = `${environment.apiUrl}/entity/${data.id_entity}/subjectProcess/${data.id_subject}/listProcess`;
    return this._http.get(url); 
  }

  listSubjectDocument(idEntity:string,keyProcess:string) {
    
    let url = `${environment.apiUrl}/entity/${idEntity}/listSubjectDocument?keyProcess=${keyProcess}`;

    return this._http.get(url);
  }

  listSubEntity(key_entity:string,partName:string) {
    
    let url = `${environment.apiUrl}/entity/${key_entity}/listIndividualPersonEntity?searchName=${partName}`;
    
    return this._http.get(url);
  }

  listSubAdministrativeEntity(key_entity:string,partName:string) {
    
    let url = `${environment.apiUrl}/entity/${key_entity}/listAdministrativeEntity?searchName=${partName}`;
    
    return this._http.get(url);
  }

  listSubEntityUser(key_entity:string) {
    
    let url = `${environment.apiUrl}/entity/${key_entity}/listEntityUser`;
    
    return this._http.get(url);
  }

//  fetchListEntityCanMoveToSubjectProcess(data:any) {
//    let url = `${environment.apiUrl}/subjectProcess/${data.id_subject}/listEntityCanMove`;
//    return this._http.get(url+`?entityWork=${data.id_entity}`); 
//  }

  fetchListSubjectProcess(idEntity:string) {
    let url = `${environment.apiUrl}/entity/${idEntity}/listSubject`;
    return this._http.get(url);  
  } 

  fetchListEntityCanConnectSubjectProcess(data:{id_subjectProcess:string,id_super:string}) {
    let url = `${environment.apiUrl}/subjectProcess/${data.id_subjectProcess}/canConnect/entity`;
    return this._http.get(url+`?idSuper=${data.id_super}`); 
  } 

  fetchListEntityCanConnectSubjectProcess_IN_AND_OUT(data:any) {
    let url = `${environment.apiUrl}/administrativeSubjectProcess/canConnect_IN_AND_OUT`;
    return this._http.put(url,data); 
  } 

  fetchListEntityPersonIn(data: { id_entity: any; id_super: any; }): any {
    let url = `${environment.apiUrl}/entity/${data.id_super}/listIndividualPersonIn`;
    return this._http.get(url+`?id_entity=${data.id_entity}`); 
  }

  fetchListProcessDetailPending(idEntity:string) {
    let url = `${environment.apiUrl}/entity/administrative/${idEntity}/process/datail/pending`;
    return this._http.get(url); 
  } 

  fetchListAdministrativeSubjectProcessProcessDetailPending(idEntity:string) {
    let url = `${environment.apiUrl}/administrativeSubjectProcess/${idEntity}/process/datail/pending`;
    return this._http.get(url); 
  } 

  receiveProcess(data:{idProcess:string,idEntity:string,idSuper:string,idEvent:string}) {

    let url = `${environment.apiUrl}/movementProcess/receive`;

    return this._http.post<any>(url,data);
  }

  rejectProcess(data:{idProcess:string,idEntity:string,idSuper:string,idEvent:string}) {

    let url = `${environment.apiUrl}/movementProcess/reject`;

    return this._http.post<any>(url,data);
  }

  fetchEntityById(id:string) {
    let url = `${environment.apiUrl}/entity/id/${id}`
    return this._http.get(url);
  }

  sendIndividualPersonToUpdate(submitEntity:any) {

    let url = `${environment.apiUrl}/entity/individualPersonOut`;

    return this._http.put(url,submitEntity);
  }

  sendLegalPersonToUpdate(submitEntity:any) {

    let url = `${environment.apiUrl}/entity/legalPerson`;

    return this._http.put(url,submitEntity);
  }

  listLevelDown(keyEntity:any) {

    let url = `${environment.apiUrl}/entity/levelDown/${keyEntity}`;

    return this._http.get(url);
    
  }

}


