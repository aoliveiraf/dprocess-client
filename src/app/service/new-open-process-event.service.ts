import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewOpenProcessEventService {

  _url = `${environment.apiUrl}/openProcessEvent`;

  constructor(private _http:HttpClient) { }

  send(data:any) {

    return this._http.post<any>(this._url,data);
  }
}
