import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewDocumentService {

  private _url = `${environment.apiUrl}/document`;

  constructor(private _http:HttpClient) { }

  send(data:any) {

    return this._http.post<any>(this._url,data);
  }

  submitAnnulation(submitData:any) {

    let url = `${environment.apiUrl}/document/annulation`;
    return this._http.post(url,submitData);
    
  }


}
