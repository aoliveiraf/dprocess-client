import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ListEntityService {

  _urlAdm = `${environment.apiUrl}/entity/administrative?searchName=`;
  _urlIndIn = `${environment.apiUrl}/entity/individualPersonIn?searchName=`;

  constructor(private _http:HttpClient) { }

  listAdministrative(data:string) {
    return this._http.get(this._urlAdm+data)
  }

  listIndividualPersonIn(data:string) {
    return this._http.get(this._urlIndIn+data)
  }

  listIndividualPersonOut(data:string) {
    let _url = `${environment.apiUrl}/entity/individualPersonOut?searchName=`;

    return this._http.get(_url+data)
  }

  listAdministrativeSibling(data:{key_entity:string,key_subjectProcess:string}) {
    let _url = `${environment.apiUrl}/entity/${data.key_entity}/listAdministrativeSibling/${data.key_subjectProcess}`;

    return this._http.get(_url);
  }

  
  updateCanEntityConnectSubjectProcess(data:any) {
    let _url = `${environment.apiUrl}/subjectProcess/canEntityConnectSubjectProcess`;
    
    return this._http.put(_url,data);

  }

  saveCanEntityConnectSubjectProcess(data:any) {
    let _url = `${environment.apiUrl}/subjectProcess/canEntityConnectSubjectProcess`;
    
    return this._http.post(_url,data);

  }

  
}
