import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private _http:HttpClient) { }

  saveAddress(address:{id:string,logradouro:string,bairro:string}) {

    let url = `${environment.apiUrl}/address`
    return this._http.post(url,address);
  }

  getAddress(cep:number) {

    let url = `${environment.apiUrl}/address/${cep}`
    return this._http.get(url);
  }
}
