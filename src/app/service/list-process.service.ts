import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ListProcessService {

  _url:string =`${environment.apiUrl}/process/entity/`;

  constructor(private _http:HttpClient) { }

  listProcessAdministrative(id:string) {

    return [
    {title:"administrative 1",description:"texto texto texto texto texto texto texto texto texto"},
    {title:"administrative 2", description:"texto texto texto texto texto texto texto texto texto texto texto texto"}]
  }
  listMyProcess(idEntity:string) {
    return this._http.get(this._url+idEntity+`/concern`); 
  }

  listProcessInBox() {
    return [
    {title:"Inbox 1",description:"texto texto texto texto texto texto texto texto texto"},
    {title:"Inbox 2", description:"texto texto texto texto texto texto texto texto texto texto texto texto"}]
  }

  detail(idProcess:string) {
    let url = `${environment.apiUrl}/process/${idProcess}/fullDetail`;

    return this._http.get(url);
  }

  listDocument(idProcess:string) {
    let url = `${environment.apiUrl}/process/${idProcess}/detail/listDocument`;

    return this._http.get(url);
  }

  sendEmail(idProcess:string) {
    let url = `${environment.apiUrl}/process/${idProcess}/protocol`;

    return this._http.get(url);
  }

  getProcess(idProcess:string) {
    let url = `${environment.apiUrl}/process/${idProcess}`;

    return this._http.get(url);
  }


}
