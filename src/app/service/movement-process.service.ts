import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class MovementProcessService {

  constructor(private _http:HttpClient) { }

  moveListProcessToEntity(data:{listIdProcess:any,id_entityFrom:string, id_entityTo:string}) {
    
    let url = `${environment.apiUrl}/movementProcess/send`;
    
    return this._http.post<any>(url,data);
  }

  getListProcess(data:{listIdProcess:any,id_entityFrom:string, id_entityTo:string}) {
    let url = `${environment.apiUrl}/movementProcess/get`;
    
    return this._http.post<any>(url,data);
  }

}
