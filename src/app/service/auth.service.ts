import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private _http:HttpClient) { }

  logout() {
    
    sessionStorage.removeItem("dataLogged");
  }

  login(data:any) {
    let _url: string = `${environment.apiUrl}/user/login`
    
    return this._http.post(_url,data);
  }

  lookupEntityAdministared(key_user:string) {
    let url: string = `${environment.apiUrl}/user/${key_user}/entity/administered`;

    return this._http.get(url);
  }

  sendUser(data:any){
    let url: string = `${environment.apiUrl}/user`

    return this._http.post(url,data); 
  }

  sendUserAdm(data:any){
    let url: string = `${environment.apiUrl}/user/adm`

    return this._http.post(url,data); 
  }

  submitUpdateUserAdmin(data:any) {

    let url = `${environment.apiUrl}/user/adm/update`;
    return this._http.put(url,data);
  }

  submitNewUserAdmin(data:any) {

    let url = `${environment.apiUrl}/user/adm/new`;
    return this._http.post(url,data);
  }

  submitNewUser(data:any) {

    let url = `${environment.apiUrl}/user`;
    return this._http.post(url,data);
  }

  submitUpdateUser(data:any) {

    let url = `${environment.apiUrl}/user`;
    return this._http.put(url,data);
  }

  deleteAdm(data:any) {

    let url = `${environment.apiUrl}/user/adm/${data.key_user}`;
    return this._http.delete(url);

  }

  getDataLogged():any {
    let dataLogged = JSON.parse(sessionStorage.getItem('dataLogged'));

    return dataLogged;

  }

  isProcessOperator() {

    let dataLogged = JSON.parse(sessionStorage.getItem('dataLogged'));

    if(!dataLogged) {
      return false;
    }
    if("ProcessOperator" in dataLogged.user.listType) {
      return true;
    }

    return false;
  }

  isConfigOperator() {

    let dataLogged = JSON.parse(sessionStorage.getItem('dataLogged'));

    if(!dataLogged) {
      return false;
    }
    if("ConfigOperator" in dataLogged.user.listType) {
      return true;
    }

    return false;
  }

  isAdministrator() {

    let dataLogged = JSON.parse(sessionStorage.getItem('dataLogged'));

    if(!dataLogged) {
      return false;
    }
    if("Administrator" in dataLogged.user.listType) {
      return true;
    }

    return false;
  }

  getToken() {

    let dataLogged =this.getDataLogged();
    
    if(!dataLogged) {

      return ""
    }

    return dataLogged.token;
  }

  lookupUserByPartName(searchName:string) {

    let url = `${environment.apiUrl}/user/searchName?searchName=${searchName}`;
    return this._http.get(url);

  }

  changePassword(key_user,key_creator,password) {

    let url = `${environment.apiUrl}/user/password/change`;
    return this._http.put(url,{key_creator:key_creator,key_user:key_user,password:password});
  
  }

  resetPassword(key_creator,key_user) {

    let url = `${environment.apiUrl}/user/password/reset`;
    return this._http.put(url,{key_creator:key_creator,key_user:key_user});
  }
}
