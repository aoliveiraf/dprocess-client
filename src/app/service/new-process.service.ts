import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class NewProcessService {

  _url = `${environment.apiUrl}/process`;

  constructor(private _http:HttpClient) { }

  send(data:any) {

    return this._http.post<any>(this._url,data);
  }
}
