import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private _http:HttpClient) { }


  getFileFromServer(url){
    console.log('Inside the service');
    console.log(url);
    

    return this._http.get(url, {responseType: 'arraybuffer'}); 
  }
  
}
