import { AuthService } from './auth.service';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  _url = `${environment.apiUrl}/entity`;

  constructor(private _http: HttpClient, private _auth: AuthService) { }

  list(data: string) {
    return this._http.get(this._url + data)
  }

  listSubjectProcess(searchName: string) {
    let url = `${environment.apiUrl}/subjectProcess/entity/${this._auth.getDataLogged().super._key}?searchName=${searchName}`;

    return this._http.get(url);
  }

  deleteHasSubjectProceesSubject(data:{key_subjectProcess:string,key_subjectDocument:string}) {
    let url = `${environment.apiUrl}/subjectProcess/${data.key_subjectProcess}/subjectDocument/${data.key_subjectDocument}`

    return this._http.delete(url);

  }

  delete(key_subjectProcess: string) {

    let url = `${environment.apiUrl}/subjectProcess/${key_subjectProcess}`;

    return this._http.delete(url);
  }

  saveSubjectProcess(data: any, status: string) {
    let url = `${environment.apiUrl}/subjectProcess`

    if (status === 'editing') {
      return this._http.put(url, data);
    } else {

      return this._http.post(url, data);
    }
  }

  listSubjectDocument(searchName: string,status:string) {
    let url = `${environment.apiUrl}/subjectDocument?searchName=${searchName}`
    if(status=='editing')
      url = `${environment.apiUrl}/subjectDocument/fit?searchName=${searchName}`
    return this._http.get(url);
  }

  saveSubjectDocument(data: any, status: string) {
    let url = `${environment.apiUrl}/subjectDocument`

    if (status === 'editing') {
     
      return this._http.put(url, data);
    } else {
      delete data._key;
      return this._http.post(url, data);
    }
  }

  listHasSubjectDocument(idSubjectProcess: string) {
    let url = `${environment.apiUrl}/subjectProcess/${idSubjectProcess}/listSubjectDocument`

    return this._http.get(url);
  }

  listCanEntityConnectSubjectProcess(idSubjectProcess: string) {

    let url = `${environment.apiUrl}/subjectProcess/${idSubjectProcess}/listCanEntityConnect`

    return this._http.get(url);
  }

  listSubjectDocumentDiffernce(idSubjectProcess: any) {
    let url = `${environment.apiUrl}/subjectDocument/${idSubjectProcess}/listDifference`;

    return this._http.get(url);
  }

  saveHasSubjectDocument(data: any) {
    let url = `${environment.apiUrl}/subjectProcess/subjectDocument`

    return this._http.post(url, data);
  }

  saveHasSubjectProcessSubject(data: any) {

    let url = `${environment.apiUrl}/subjectProcess/hasSubjectDocument`

    return this._http.post(url, data);

  }

}
