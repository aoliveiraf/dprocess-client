import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class SearchProcessService {

  constructor(private _http:HttpClient) { }

  searchProcessById(id_process:string) {
    let url = `${environment.apiUrl}/searchProcess/id/${id_process}`;
    return this._http.get(url);
  }

  searchProcess(query:any) {
    let url = `${environment.apiUrl}/searchProcess`;
    return this._http.put(url,query);

  }
}
