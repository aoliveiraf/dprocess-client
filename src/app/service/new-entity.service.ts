import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewEntityService {

  _url = `${environment.apiUrl}/entity`;

  constructor(private _http:HttpClient) { }

  send(data) {
    
    return this._http.post<any>(this._url,data);
    
  }
}
