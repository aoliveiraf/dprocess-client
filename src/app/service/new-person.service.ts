import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NewPersonService {

  

  constructor(private _http:HttpClient) { }

  sendIndividualPersonIn(data) {
    let _url:string = `${environment.apiUrl}/entity/individualPersonIn`;  
    
    return this._http.post<any>(_url,data);
    
  }

  sendIndividualPersonOut(data) {
    let _url:string = `${environment.apiUrl}/entity/individualPersonOut`;  

    return this._http.post<any>(_url,data);
  }

  sendUnitIn(data:any) {
    let _url:string = `${environment.apiUrl}/entity/administrative`;  

    return this._http.post<any>(_url,data);
  }

  sendLegalPerson(data) {
    let _url:string = `${environment.apiUrl}/entity/legalPerson`;  

    return this._http.post<any>(_url,data);
  }


}
