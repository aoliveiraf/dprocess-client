import { EntityService } from './../service/entity.service';
import { AuthService } from './../service/auth.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-disconnect-person',
  templateUrl: './disconnect-person.component.html',
  styles: ['']
})
export class DisconnectPersonComponent implements OnInit {

  constructor(private _fb: FormBuilder, private _auth: AuthService, private router: Router, private route: ActivatedRoute, private serviceEntity: EntityService) { }

  listEntity: any;
  listUnit: any;
  _listSubEntity: any;
  canDeleteError = false;
  noConnectPerson:boolean=false;

  sForm = this._fb.group(
    {
      id: ['']
    }
  );

  registrationForm = this._fb.group(
    {
      key_super: [''],
      key_entity: ['']
    }
  )

  ngOnInit() {

    this.listSubEntityAdm('')
  }

  onChangeEntity() {

    this.listSubEntity(this.registrationForm.value.key_super);
  }

  listSubEntity(key_entity: string) {

    this.serviceEntity.listSubEntity(key_entity, '').
      subscribe(
        (success2: any) => {
          this._listSubEntity = success2;
        },
        error1 => console.log('Error', error1)
      )
  }

  listSubEntityAdm(partName: string) {

    this._auth.lookupEntityAdministared(this._auth.getDataLogged().user._key).
      subscribe(
        (success1: any) => {
          let entity = success1.pop();

          this.serviceEntity.listSubAdministrativeEntity(entity._key, partName).
            subscribe(
              (success2: any) => {
                this.listUnit = success2;
              },
              error1 => console.log('Error', error1)
            )
        },
        error2 => {
          error2 => console.log('Error', error2)
        }
      )

  }

  onSubmit() {
    this.serviceEntity.fetchEntityById(this.sForm.value.id).
      subscribe(
        success => {
          this.listEntity = success;
        },
        error => {
          console.log('error', error)
        }
      )
  }

  loadForm(data: any) {
    this.sForm.setValue({
      id: data.id,
      name: data.name
    })
  }

  disconnect(entity: any) {

    this.serviceEntity.canDelete(entity._key).subscribe(
      (success: any) => {
        let data = success.pop();
        let _canDisconnect = (typeof data.listProcess !== undefined && data.listProcess.length == 0) &&
          (typeof data.listSubEntity !== undefined && data.listSubEntity.length == 0) &&
          (typeof data.listPromiseProcess !== undefined && data.listPromiseProcess.length == 0)

        if (_canDisconnect) {
          let submitData = {
            key_entity:entity._key,
            key_creator:this._auth.getDataLogged().entity._key
          }
          this.serviceEntity.disconnect(submitData).subscribe(
            success => {
              this.listSubEntity(this.registrationForm.value.key_super);
            },
            error => {
              console.log('error', error);
            }
          )
        } else {
          this.canDeleteError = true;
        }
      },
      error => console.log('error', error)
    )
  }


}
