import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardEntityInComponent } from './dashboard-entity-in.component';

describe('DashboardEntityInComponent', () => {
  let component: DashboardEntityInComponent;
  let fixture: ComponentFixture<DashboardEntityInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardEntityInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardEntityInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
