import { SearchProcessService } from './../service/search-process.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-search-process',
  templateUrl: './search-process.component.html',
  styles: ['']
})
export class SearchProcessComponent implements OnInit {
  registrationForm =  this._fb.group({
    id_process:[''],
    partReqName:[''],
    partIndName:[''],
    partAdmName:[''],
    partNameSubject:['']

  })
  listDetailProcess:any;

  constructor(private _auth:AuthService, private _fb:FormBuilder,private searchProcoess:SearchProcessService) { }

  ngOnInit() {
  }

  showORhideEvent(id) {
    console.log(id);
    
    var x = document.getElementById("event"+id);

    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  showORhide(id) {
    console.log(id);
    
    var x = document.getElementById("list"+id);

    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  onSubmit() {
    let data = this.registrationForm.value;
    data.id_super = this._auth.getDataLogged().super._key;
    data.processActive="";
    this.searchProcoess.searchProcess(data)
    .subscribe(
      success => {
        this.listDetailProcess = success;
    },
      error => console.log('error',error)      
    )
  }

}
