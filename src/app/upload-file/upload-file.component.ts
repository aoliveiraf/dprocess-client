import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, ElementRef } from '@angular/core';
import {  FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';


@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styles: ['']
})
export class UploadFileComponent implements OnInit {

  constructor(private route:ActivatedRoute, private el: ElementRef) { }
  id:string;
  afuConfig = {
    multiple: false,
    formatsAllowed: ".pdf",
    uploadAPI:  {
      url:`${URL}?idProcess=123`
    },
    hideProgressBar: false,
    hideResetBtn: false,
    hideSelectBtn: false,
    theme: "dragNDrop",
    replaceTexts: {
      selectFileBtn: 'Selecione o arquivo',
      resetBtn: 'Resetar',
      uploadBtn: 'Enviar',
      dragNDropBox: 'Araste e Solte',
      attachPinBtn: 'Anexar Arquivo...',
      afterUploadMsg_success: 'Envio com sucesso!',
      afterUploadMsg_error: 'Envio falhou!'
    }
};
  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
  }

  onSubmit() {
  }

}
