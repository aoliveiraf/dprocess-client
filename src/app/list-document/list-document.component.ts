import { NewDocumentService } from '../service/new-document.service';
import { FormBuilder } from '@angular/forms';
import { ListProcessService } from '../service/list-process.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EntityService } from '../service/entity.service';
import { AuthService } from '../service/auth.service';
import { environment } from 'src/environments/environment';
import { AngularEditorConfig } from '@kolkov/angular-editor';


@Component({
  selector: 'app-list-document',
  templateUrl: './list-document.component.html',
  styles: ['']
})
export class ListDocumentComponent implements OnInit {

  _listDocument: any;
  _idProcess: any;
  _readonly:boolean=true;
  _hasShow = "";
  nameFrame = "nameFrame"

  @Input() set readonly(value:boolean) {
    this._readonly = value;
  };
  @Input() set listDocument(listDocument: any) {
    this._listDocument = listDocument;
    
    this.addUploadComponent(this._listDocument);
  };
  @Input() set idProcess(idProcess: any) {
    this._idProcess = idProcess;
    
    this.lookupListDocument();
  };

  @Output() public annulationEvent = new EventEmitter();


  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '5',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',
  };

  currentProcess: any;
  suggestionText: string;
  lastId = -1;
  constructor(private _serviceEntity: EntityService, private _auth: AuthService, private _fb: FormBuilder, private _route: ActivatedRoute, private _serviceProcess: ListProcessService, private _serviceDocument: NewDocumentService) { }

  registrationForm = this._fb.group(
    {
      id_subject: [''],
      text: ['']
    })
  lookupListDocument() {
    
    this._serviceProcess.listDocument(this._idProcess)
      .subscribe(
        success => {

          this._listDocument = success || [];
          
          this.addUploadComponent(this._listDocument);
          
        },
        error => console.log('Error', error)
      )
  }

  showORhide(id) {
    var x = document.getElementById(id);

    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  showORhideDocument(id) {
    if(this._hasShow==id) {
      this._hasShow="";
    } else {
      this._hasShow=id;
    }

  }


  onShowText(data, index) {

    if (this.lastId != -1) {
      this.showORhide(this.lastId)
    }
    this.showORhide(index)
    this.lastId = index;
    this.registrationForm.setValue({
      id_subject: [''],
      text: [''],
      showText: data
    });

  }
  docUpload(event: any) {
    
    if (event.status === 200) {
      this.lookupListDocument();
    }
  }
  submitEvent(data: any) {
    this.lookupListDocument();
    this.annulationEvent.emit(data);
  }


  createUploader(keyDocument: string, keySubject: string): any {
    //  let fileName = `documento.${keyDocument}.assunto.${keySubject}`;
    return {
      multiple: false,
      //      formatsAllowed: ".pdf,.xlsx,.xls,.ods,.docx,.doc",
      maxSize: "20",
      formatsAllowed: ".pdf",
      uploadAPI: {
        url: `${environment.uploadUrl}/upload?keyDocument=${keyDocument}&keyProcess=${this._idProcess}`
      },
      theme: "dragNDrop",
      //      theme: "attachPin",
      hideProgressBar: false,
      hideResetBtn: true,
      hideSelectBtn: false,
      replaceTexts: {
        dragNDropBox: 'Araste um documento e solte aqui.',
        attachPinBtn: "Anexar um documento.",
        selectFileBtn: 'Anexar um documento..',
        uploadBtn: 'Enviar o documento.',
        afterUploadMsg_success: 'Envio Concluído!',
        afterUploadMsg_error: 'Falha no Envio!'
      }
    }

  }
  addUploadComponent(listDocument: any) {
    if(!!listDocument) {
      listDocument.forEach(element => {
        if (!element.document.path || !(element.document.text !== '' && element.subject._type !== 'Ordinary')) {
          element.uploader = this.createUploader(element.document._key, element.subject._key);
        }
      });  
    }
  }

  ngOnInit() {
  }
}

