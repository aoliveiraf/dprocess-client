import { AuthService } from './service/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: ['']
})
export class AppComponent {
  title = 'DigProcess';

  constructor(private acRoute:ActivatedRoute, private route:Router, private _auth:AuthService) {}

  OnInit() {

  }

  logout() {

    this._auth.logout();
    this.route.navigate(['/login']);
  }

  redirectSuper() {
    let id_entity = this._auth.getDataLogged().super._key;
    
    this.route.navigate([`/process/entity/${id_entity}/pending`])
    return false;
  } 

  btnclick() {

    let x = document.getElementById('navbarSupportedContent');

    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }
  clickGenericLink(link:string) {

    let x = document.getElementById('navbarSupportedContent');

    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }

    this.route.navigate([link])
    return false;

  }
  redirectEntity() {


    let x = document.getElementById('navbarSupportedContent');

    x.style.display='none'

    let id_entity = this._auth.getDataLogged().entity._key;
    
    this.route.navigate([`/process/entity/${id_entity}/pending`])
    return false;
  } 

  
}
