import { AuthService } from './../service/auth.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { log } from 'util';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: ['']
})
export class LoginComponent implements OnInit {

  constructor(private _fb:FormBuilder,private _authService:AuthService,private router:Router) { }

  errorMesage:string='';
  registrationForm = this._fb.group({
    name: [''],
    password: ['']
  });

  ngOnInit() {
  }

  onSubmit() {
    
    this._authService.login(this.registrationForm.value)
    .subscribe(
      res => {
        sessionStorage.setItem("dataLogged",JSON.stringify(res));
        
//        localStorage.setItem("dataLogged",JSON.stringify(res));
        this.errorMesage = '';
        this.router.navigate(['/first']);
      },
      error => {
        this.errorMesage = 'Usuário ou senha inválida!'
      }
        
    )
  }

}
