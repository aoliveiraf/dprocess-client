import { AuthService } from './../service/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styles: ['']
})
export class FirstComponent implements OnInit {

  constructor(private _auth:AuthService) { }

  user:any;
  entity:any;
  _super:any;

  ngOnInit() {
  
    this.user = this._auth.getDataLogged().user;
    this.entity = this._auth.getDataLogged().entity;
    this._super = this._auth.getDataLogged().super;

  }



}
