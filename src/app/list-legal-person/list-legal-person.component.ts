import { EntityService } from './../service/entity.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-legal-person',
  templateUrl: './list-legal-person.component.html',
  styles: ['']
})
export class ListLegalPersonComponent implements OnInit {

  constructor(private _fb: FormBuilder, private router:Router, private route: ActivatedRoute, private serviceEntity: EntityService) { }

  _listEntity: any;
  sForm = this._fb.group(
    {
      id: [''],
      name: ['']
    }
  );

  ngOnInit() {

    if (!!this.route.snapshot.params.id) {
      
      this.loadForm({ id: this.route.snapshot.params.id,name:''});
      this.onSubmit();
    }
  }

  loadForm(data: any) {
    this.sForm.setValue({
      id: data.id,
      name: data.name
    })
  }

  onThrowToProcess(entity:any) {

    this.router.navigate([`/process/to/entity/${entity.id}`])
  }

  onEdit(entity:any) {
    
    if(entity.id.length == 11) {
      this.router.navigate([`/entity/legalPerson/${entity.id}`])
    } 
  }

  onSubmit() {

    this.serviceEntity.fetchEntityById(this.sForm.value.id).
      subscribe(
        (success: any) => {
          this._listEntity = success;
        },
        error => {
          console.log('error', error)
        }
      )
  }

}
