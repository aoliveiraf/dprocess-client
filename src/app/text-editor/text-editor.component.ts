import {
	Component,
	AfterViewInit
} from '@angular/core';
//import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
//import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment';
import { AngularEditorConfig } from '@kolkov/angular-editor';

@Component({
	selector: 'app-text-editor',
	templateUrl: './text-editor.component.html',
	styleUrls: ['./text-editor.component.css']
})
export class TextEditorComponent implements AfterViewInit {

//	public editor = ClassicEditor;

editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '0',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',
};

	public ngAfterViewInit() {
/*		this.editor
		ClassicEditor
		.create( document.querySelector( '#editor' ), {
		//	plugins: [ Alignment ],     // <--- MODIFIED
			toolbar: [ 'bold', 'italic', 'alignment' ]                       // <--- MODIFIED
		} )
		.then( editor => {
			console.log( 'Editor was initialized', editor );
		} )
		.catch( error => {
			console.error( error.stack );
		} );	
		*/
	}

}