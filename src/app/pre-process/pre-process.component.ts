import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pre-process',
  templateUrl: './pre-process.component.html',
  styles: ['']
})
export class PreProcessComponent implements OnInit {

  constructor() { }

  entity:any;
  showProcess:boolean=false;
  showPerson:boolean=false;
  showMensage:boolean=false;
  label:string;

  ngOnInit() {
    this.label =" - Localizar Requerente"
  }

  reciveNewEntityEvent(data:any) {
    this.entity = data;

    if(typeof data==undefined) {
      this.showMensage=true;     
      this.showPerson = false;
      this.showProcess = false; 
    } else {
      this.showMensage=false;     
      this.showPerson = true;
      this.showProcess = false; 
    }
  }

  onNew() {

    this.showMensage=false;     
    this.showPerson = true;
    this.showProcess = false; 

  }

  onNewSearch() {
    this.showMensage=false;     
    this.showPerson = false;
    this.showProcess = false; 


  }
  reciverFeedback(resposta:any) {
    
    this.entity = resposta;

    if(resposta==null) {
      this.showMensage=true;     
      this.showPerson = false;
      this.showProcess = false; 
    } else {
      this.showMensage=false;     
      this.showPerson = true;
      this.showProcess = false; 
    }

  }
}
