import { AuthService } from './../service/auth.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NewPersonService } from '../service/new-person.service';

@Component({
  selector: 'app-unit-out',
  templateUrl: './unit-out.component.html',
  styles: ['']
})
export class UnitOutComponent implements OnInit {

  constructor(private _auth: AuthService,private _fb:FormBuilder, private serviceNewPerson:NewPersonService) { }

  registrationForm = this._fb.group({
    name: [''],
    id: [''],
    id_creator: ['']
  });
  
  ngOnInit() {
  }

  onSubmit() {
    let dataLogged = this._auth.getDataLogged();

    let submitEntity = this.registrationForm.value;
    submitEntity.id_creator = dataLogged.entity._key;
    
    this.serviceNewPerson.sendLegalPerson(submitEntity)
    .subscribe(
      success => console.log("Success!",success),
      error => console.log("Error!", error)
    )
  }

}
