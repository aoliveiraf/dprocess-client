import { FileService } from './../service/file.service';
import { Component, OnInit, Input } from '@angular/core';
import { environment } from './../../environments/environment';
import {DomSanitizer} from '@angular/platform-browser'

@Component({
  selector: 'app-viewer-pdf',
  templateUrl: './viewer-pdf.component.html',
  styleUrls: ['./viewer-pdf.component.css']
})
export class ViewerPdfComponent implements OnInit {

  constructor(private sanitizer:DomSanitizer,private fileService:FileService) { }

  isShow:boolean=false;
  _urlDoc:any;
  @Input() public set urlDoc(url:string) {
    
    this._urlDoc = this.sanitizer.bypassSecurityTrustResourceUrl(url);

  }

  @Input() public idDoc:string;



  ngOnInit() {

  }

  showPdf() {

    if(this.isShow) {
      this.isShow = false;
    } else {
      this.isShow = true;
    }
  }
 
}
 