import { FileService } from './../service/file.service';
import { Component, OnInit, Input } from '@angular/core';
import { environment } from './../../environments/environment';
import {DomSanitizer} from '@angular/platform-browser'

@Component({
  selector: 'app-viewer-pdf',
  templateUrl: './viewer-pdf.component.html',
  styleUrls: ['./viewer-pdf.component.css']
})
export class ViewerPdfComponent implements OnInit {

  constructor(private sanitizer:DomSanitizer,private fileService:FileService) { }

  urlPdf:any;
  @Input() public keyDocument:string;

  ngOnInit() {

  }
  showORhide() {
    var x = document.getElementById(this.keyDocument);
    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }
/*  getPdfFile() {
    AWS.config.credentials = new AWS.Credentials({
      accessKeyId: 'AKIAZ7A5Y2GPRGRCOYH5', secretAccessKey: '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'
    });

    const params = {
      Bucket: 'dprocess-upload',
      Key: '8607350-9797897-02111370495-IRPF-2019-2018-retif-imagem-recibo.pdf'
    };
    let s3 = new AWS.S3();

    s3.getObject(params, (err, data) =>{
      if (err) {
        console.error(err); // an error occurred
      } else {
        console.log(data);
        
        //this.urlPdf = this.sanitizer.bypassSecurityTrustResourceUrl(this.getUrlToPdf(data,"application/pdf"));
      }
    });
  } 
*/


  getPdfFile() {
    
//    let url = `${environment.apiUrl}/upload/document/${this.keyDocument}`;
    let url = `${environment.uploadUrl}/upload/${this.keyDocument}`;

//    this.fileService.getFileFromServer(url).subscribe(
//      responseData => {
          //catch response as array buffer
//          console.log('response data');
          
//          console.log(responseData);

          
       //   this.urlPdf = this.sanitizer.bypassSecurityTrustResourceUrl(this.getUrlToPdf(responseData,"application/pdf"));
//          this.showPdfFile(responseData, "application/pdf");
//      }, error => {
          //catch error if any
//          console.log('inside the error');
          
//          console.log(error);
//      });
    
  }
  
 
  getUrlToPdf(data: any, type: string):any {
    var blob = new Blob([data], { type: type });
    var url = window.URL.createObjectURL(blob);
    return url;
  }
  
  showPdfFile(data: any, type: string) {
      var blob = new Blob([data], { type: type });
      var url = window.URL.createObjectURL(blob);
      
      window.open(url, "_parent", "toolbar=false,scrollbars=yes,resizable=false,top=0,left=100,width=400,height=400");
//      window.open(url);
  }

}
 