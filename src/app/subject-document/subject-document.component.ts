import { AuthService } from './../service/auth.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { SubjectService } from '../service/subject.service';

@Component({
  selector: 'app-subject-document',
  templateUrl: './subject-document.component.html',
  styles: ['']
})
export class SubjectDocumentComponent implements OnInit {

  constructor(private _auth:AuthService, private _fb:FormBuilder,private _serviceSubject:SubjectService) { }

  status:string = 'creating';
  listing:boolean = false;
  _submit: boolean = false;
  subjectDocumentForm = this._fb.group({
    name:[''],
    _key:['']
  }) 
  _listSubjectDocument:any;
  ngOnInit() {
    this.listSubjectDocument('');
  }
  listSubjectDocument(searchName:string) {
    this._serviceSubject.listSubjectDocument(searchName,this.status).subscribe(
      (success:any) => {
        this._listSubjectDocument = success
        if(success.length >0) {
          this.listing = true;
        } else {
          this.listing = false;
        }
        this.testDisabled(success,searchName)

      },
      error => console.log('Error!',error)
    )

  }


  testDisabled(listSubjectDocument:any [],partName:string) {

    const finded = listSubjectDocument.find((element:any)=>{
    
      return element.name.toLowerCase() == partName.toLowerCase();
    }) 
    
    this._submit = !finded && this.subjectDocumentForm.valid
  }

  onKeyUpName() {

    this.listSubjectDocument(this.subjectDocumentForm.value.name);
  }

  changingName() {

    this.listSubjectDocument(this.subjectDocumentForm.value.name);
  }

  onEdit(subjectDocument: any) {
    this.status = 'editing';
    this.subjectDocumentForm.setValue({
      name: subjectDocument.name,
      _key: subjectDocument._key
    })
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  onNew() {
    this.status = 'creating';
    this.subjectDocumentForm.setValue({
      name:'',
      _key:''
    })
  }


  onSubmit() {
    let submitData = this.subjectDocumentForm.value;

    this._serviceSubject.saveSubjectDocument(submitData,this.status).subscribe(
      (success:any) => {
        this.onNew();
        this.listSubjectDocument(success.name);
      },
      error => console.log('Error!',error)
      
    )
  }

}
