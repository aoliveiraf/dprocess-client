import { FormBuilder } from '@angular/forms';
import { AuthService } from './../service/auth.service';
import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { NewPersonService } from '../service/new-person.service';
import { EntityService } from '../service/entity.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-individual-person',
  templateUrl: './individual-person.component.html',
  styles: ['']
})
export class IndividualPersonComponent implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, private _serviceEntity: EntityService, private _auth: AuthService, private _fb: FormBuilder, private serviceNewPerson: NewPersonService) { }
  address: any;
  status: string;
  legalperson: boolean = false;
  idExist: boolean = false;
  _person: any;

  @Output() public newEntityEnvent = new EventEmitter();
  @Input() set person(p: any) {
    this.registrationForm.setValue({
      id_creator: this._auth.getDataLogged().entity._key || '',
      name: p.name || '',
      nameMom:p.nameMom || '',
      id: p.id || '',
      email: p.email || '',
      phone: p.phone || '',
      complemento: p.address.complemento || '',
      firstId: p.address.firstId || '',
//      sex: p.sex || '',
//      date: p.date || ''
    })
    this.address = p.address;
    this._person = p;
    this.status = "editing";

  }
  get person(): any {
    return this._person;
  }

  registrationForm = this._fb.group({
    name: [''],
    nameMom:[''],
    id: [''],
    id_creator: [''],
    email: [''],
    phone: [''],
    complemento: [''],
    firstId: [''],
//    sex: [''],
//    date: ['']
  });
  individual: boolean = this.registrationForm.value.id.length < 14;
  showButtonToProcess: boolean = false;
  ngOnInit() {

    if (this.route.routeConfig.path.indexOf('process')>=0) {
      this.showButtonToProcess = true;
    }

    if (!!this.route.snapshot.params.id) {
      this._serviceEntity.fetchEntityById(this.route.snapshot.params.id).
        subscribe(
          (success: any) => {
            this.person = success.pop();
          }
        )
    } else {
      this.status = "creating";
    }
  }

  reciverEventAddress(resposta: any) {

    this.address = resposta;
  }

  changeCpf() {
    console.log(this.registrationForm.value.id.length);
    
    if (this.registrationForm.value.id.length == 11) {
      this._serviceEntity.fetchEntityById(this.registrationForm.value.id).
        subscribe(
          (success: any) => {
            if (success.length > 0) {
              this.idExist = true;
            } else {
              this.idExist = false;
            }
          }
        )
    } else if (this.idExist) {
      this.idExist = false;
    }
  }

  onSubmit() {

    if(this.showButtonToProcess) {

      this.onSubmitToProcess();
    } else {

      this.onSubmitToList();
    }
  }
  onSubmitToList() {
    let dataLogged = this._auth.getDataLogged();

    let submitEntity = this.registrationForm.value;
    if(dataLogged == null) {
      submitEntity.id_creator = "neutal/0";  
    } else {
      submitEntity.id_creator = dataLogged.entity._key;
    }
    submitEntity.id_address = this.address._key;

    if (!!this.person && !!this.person._key) {

      submitEntity._key = this.person._key;

      this._serviceEntity.sendIndividualPersonToUpdate(submitEntity)
        .subscribe(
          (success: any) => {
            this.newEntityEnvent.emit(success);
            this.router.navigate([`/person/${this.person.id}/list`]);
          },
          error => console.log("Error!", error)
        )

    } else {

      this.serviceNewPerson.sendIndividualPersonOut(submitEntity)
        .subscribe(
          success => {
            this.newEntityEnvent.emit(success);
            this.router.navigate([`/person/${success.id}/list`]);
          },
          error => console.log("Error!", error)
        )
    }
  }

  onSubmitToProcess() {
    let dataLogged = this._auth.getDataLogged();
     

    let submitEntity = this.registrationForm.value;
    if(dataLogged == null) {
      submitEntity.id_creator = "neutal/0";  
    } else {
      submitEntity.id_creator = dataLogged.entity._key;
    }
    submitEntity.id_address = this.address._key;

    if (!!this.person && !!this.person._key) {

      submitEntity._key = this.person._key;

      this._serviceEntity.sendIndividualPersonToUpdate(submitEntity)
        .subscribe(
          (success: any) => {
            this.newEntityEnvent.emit(success);
            this.router.navigate([`/process/to/entity/${this.person.id}`]);
          },
          error => console.log("Error!", error)
        )

    } else {

      this.serviceNewPerson.sendIndividualPersonOut(submitEntity)
        .subscribe(
          success => {
            this.newEntityEnvent.emit(success);
            this.router.navigate([`/process/to/entity/${this.person.id}`]);
          },
          error => console.log("Error!", error)
        )
    }
  }
}
