import { EntityService } from './../service/entity.service';
import { AuthService } from './../service/auth.service';
import { ListEntityService } from './../service/list-entity.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NewPersonService } from '../service/new-person.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styles: ['']
})
export class UserComponent implements OnInit {


  constructor(private _fb:FormBuilder, private serviceEntity: EntityService, private _auth:AuthService) { }

  listUnit:any;
  registrationForm = this._fb.group({
    name: [''],
    password: [''],
    key_unit: ['']
  });

  ngOnInit() {

    this.listSubEntity('')
  }

  listSubEntity(partName: string) {

    this._auth.lookupEntityAdministared(this._auth.getDataLogged().user._key).
    subscribe(
      (success1:any) => {
        let entity = success1.pop();
        
        this.serviceEntity.listSubEntity(entity._key,partName).
        subscribe(
          (success2: any) => {
            this.listUnit = success2;
          },
          error1 => console.log('Error', error1)
        )  
      },
      error2 => {
        error2 => console.log('Error', error2)
      }
    )

  }

  resetForm() {

    this.registrationForm.setValue({
      name:'',
      password:'',
      key_unit:''
  
    })
  }
  onSubmit() {
    let submitEntity = this.registrationForm.value;
    submitEntity.key_creator = this._auth.getDataLogged().entity._key;
    
    this._auth.sendUserAdm(submitEntity)
    .subscribe(
      success => {
        this.resetForm();
    },
      error => console.log("Error!", error)
    )
    
  }

}
