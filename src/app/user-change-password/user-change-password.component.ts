import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-user-change-password',
  templateUrl: './user-change-password.component.html',
  styles: ['']
})
export class UserChangePasswordComponent implements OnInit {

  constructor(private _fb:FormBuilder,private _auth: AuthService) { }

  notCheck = true;
  registrationForm = this._fb.group({
    name: [''],
    password: [''],
    passwordCheck: ['']
  });

  ngOnInit() {
  }

  onKeyUp() {    
        
    if(this.registrationForm.value.password!=="" && this.registrationForm.value.password == this.registrationForm.value.passwordCheck) {
      this.notCheck = false;
    } else {
      this.notCheck = true;
    }
  }

  onSubmit() {

    const key_user = this._auth.getDataLogged().user._key;
    const key_creator = this._auth.getDataLogged().entity._key;

    const password = this.registrationForm.value.password;

    this._auth.changePassword(key_user,key_creator,password).subscribe(
      success => console.log("password has been changed."),
      error => console.log('error', error)
    )
  }
}
