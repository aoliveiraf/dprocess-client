import { AuthService } from './../service/auth.service';
import { SubjectService } from './../service/subject.service';
import { ListEntityService } from './../service/list-entity.service';
import { FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NewProcessService } from '../service/new-process.service';
import { Router } from '@angular/router';
import { EntityService } from '../service/entity.service';

@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styles: ['']
})
export class ProcessComponent implements OnInit {

  listEntity:any = [];
  currentEntity:any;
  listSubject:any = [];
  currentIdSubject:any;
  
  constructor(private _auth: AuthService,private _fb:FormBuilder, private _serviceEntity:EntityService, private _authService:AuthService, private router:Router,private serviceListEntity:ListEntityService, private serviceSubject: SubjectService, private _newService:NewProcessService) { }

  registrationForm = this._fb.group({
    id_subject:[''],
    id_creator:[''],
    description: [''],
    id:[''],
    id_concernedOut: ['']
  });

  ngOnInit() {
    this.fetchListSubjectProcess();
  }

  fetchListSubjectProcess() {
    this._serviceEntity.fetchListSubjectProcess(this._authService.getDataLogged().super._key)
    .subscribe(
      success=> this.listSubject=success,
      error=> console.log("error!",error)
    )
  }
  onKeyUp() {
    this.serviceListEntity.listIndividualPersonOut(this.registrationForm.value.id_concernedOut)
    .subscribe(
      success=> this.listEntity=success,
      error=> console.log("error!",error)
    )
    
  }

/*  onKeyUpSubject() {
//    this.serviceSubject.list(this.registrationForm.value.id_subject)
    this._serviceEntity.fetchListSubjectProcess(this._authService.getDataLogged().entity._key)
    .subscribe(
      success=> this.listSubject=success,
      error=> console.log("error!",error)
    )
    
  }
*/
  onSubjectChange() {
    this.currentIdSubject = this.registrationForm.value.id_subject;
  }

  onEntityChange() {
    this.currentEntity = this.getSelecteEntitytByName(this.registrationForm.value.id_concernedOut);
    this.updateFormEntity();
  }
  updateFormEntity() {
    this.registrationForm.setValue({
      id_subject: this.registrationForm.value.id_subject,
      description: this.registrationForm.value.description,
      id: this.registrationForm.value.id,
      id_creator: this.registrationForm.value.id_creator,
      id_concernedOut: this.currentEntity.name
    });
  }
/*
  updateFormSubject() {
    this.registrationForm.setValue({
      id_subject: this.currentSubject.name,
      description: this.registrationForm.value.description,
      id: this.registrationForm.value.id,
      id_creator: this.registrationForm.value.id_creator,
      id_concernedOut: this.registrationForm.value.id_concernedOut
    });
  }
*/
  lookupAdministrative() {

    return '';
  }
  getSelecteEntitytByName(selectedKey: string): any {
    return this.listEntity.find(entity => entity._key === selectedKey);
  }

  getSelectSubjectByName(selectedKey: string): any {
    return this.listSubject.find(subject => subject._key === selectedKey);
  }

  onSubmit() {
    let dataLogged = this._auth.getDataLogged();

    let submitProcess = this.registrationForm.value;
    submitProcess.id_concernedOut = this.currentEntity._key;
    submitProcess.id_creator = dataLogged.entity._key;
    submitProcess.id_subject = this.currentIdSubject;
    submitProcess.id_administrative = dataLogged.super._key;
    
    this._newService.send(submitProcess)
      .subscribe(
        success => this.router.navigate(['/painelControlProcess',success._key]),
        error => console.log('Error!',error)
      )
  }

}
