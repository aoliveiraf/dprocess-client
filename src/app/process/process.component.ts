import { AuthService } from './../service/auth.service';
import { SubjectService } from './../service/subject.service';
import { ListEntityService } from './../service/list-entity.service';
import { FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { NewProcessService } from '../service/new-process.service';
import { Router, ActivatedRoute } from '@angular/router';
import { EntityService } from '../service/entity.service';

@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styles: ['']
})
export class ProcessComponent implements OnInit {

  listEntity:any = [];
  @Input() public entity:any;
  listSubject:any = [];
  currentIdSubject:any;
  errorMessage = '';
  constructor(private _auth: AuthService,private route:ActivatedRoute, private _fb:FormBuilder, private _serviceEntity:EntityService, private _authService:AuthService, private router:Router,private serviceListEntity:ListEntityService, private serviceSubject: SubjectService, private _newService:NewProcessService) { }

  registrationForm = this._fb.group({
    id_subject:[''],
    id_creator:[''],
    description: [''],
    id:[''],
    id_concernedOut: ['']
  });

  ngOnInit() {
    this._serviceEntity.fetchEntityById(this.route.snapshot.params.id)
    .subscribe(
      (success: any) => {
        this.entity = success.pop();
      },
      error => {
        console.log('error', error)
      }
    )

    this.fetchListSubjectProcess();
  }

  fetchListSubjectProcess() {
    this._serviceEntity.fetchListSubjectProcess(this._authService.getDataLogged().super._key)
    .subscribe(
      success=> this.listSubject=success,
      error=> console.log("error!",error)
    )
  }
  onKeyUp() {
    this.serviceListEntity.listIndividualPersonOut(this.registrationForm.value.id_concernedOut)
    .subscribe(
      success=> this.listEntity=success,
      error=> console.log("error!",error)
    )
    
  }


  onSubjectChange() {
    this.currentIdSubject = this.registrationForm.value.id_subject;
  }

  getSelectSubjectByName(selectedKey: string): any {
    return this.listSubject.find(subject => subject._key === selectedKey);
  }

  onSubmit() {
    let dataLogged = this._auth.getDataLogged();

    let submitProcess = this.registrationForm.value;
    submitProcess.id_concernedOut = this.entity._key;
    submitProcess.id_creator = dataLogged.entity._key;
    submitProcess.id_subject = this.currentIdSubject;
    submitProcess.id_administrative = dataLogged.super._key;
    
    this._newService.send(submitProcess)
      .subscribe(
        success => this.router.navigate(['/painelControlProcess',success._key]),
        error => console.log('Error!',error)
      )
  }

}
