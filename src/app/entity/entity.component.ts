import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NewEntityService } from '../service/new-entity.service';

@Component({
  selector: 'app-entity',
  templateUrl: './entity.component.html',
  styleUrls: ['./entity.component.css']
})
export class EntityComponent implements OnInit {


  constructor(private _fb: FormBuilder, private _newService: NewEntityService) { }

  registrationForm = this._fb.group({
    name: ['Secretaria 1'],
    id: [''],
    id_creator: ['15090',{disabled: true}]
  })
  ngOnInit() {
  }

  onSubmit() {
    this._newService.send(this.registrationForm.value)
      .subscribe(
        response => console.log('Success!', response),
        error => console.log('Error!', error)
      )
//    console.log(this.registrationForm.value);

  }
}
