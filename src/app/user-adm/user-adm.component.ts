import { FormBuilder } from '@angular/forms';
import { AuthService } from './../service/auth.service';
import { EntityService } from './../service/entity.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-adm',
  templateUrl: './user-adm.component.html',
  styles: ['']
})
export class UserAdmComponent implements OnInit {

  constructor(private _fb:FormBuilder, private serviceEntity: EntityService, private _auth:AuthService) { }

  listUnit:any;
  registrationForm = this._fb.group({
    name: [''],
    password: [''],
    key_unit: ['']
  });

  ngOnInit() {
    this.listSubEntity('')
  }

  showPassword(idComponent: string, event: any) {
    let x: any = document.getElementById(idComponent);
    if (x.type === "password") {
      x.type = "text";
      event.target.className = "fa fa-eye";
    } else {
      x.type = "password";
      event.target.className = "fa fa-eye-slash";

    }
  }


  listSubEntity(partName: string) {

    this._auth.lookupEntityAdministared(this._auth.getDataLogged().user._key).
    subscribe(
      (success1:any) => {
        let entity = success1.pop();
        this.serviceEntity.listSubAdministrativeEntity(entity._key,partName).
        subscribe(
          (success2: any) => {
            this.listUnit = success2;
          },
          error1 => console.log('Error', error1)
        )  
      },
      error2 => {
        error2 => console.log('Error', error2)
      }
    )

  }

  resetForm() {

    this.registrationForm.setValue({
      name:'',
      password:'',
      key_unit:''
  
    })
  }
  onSubmit() {
    let submitEntity = this.registrationForm.value;
    submitEntity.key_creator = this._auth.getDataLogged().entity._key;
    
    this._auth.sendUserAdm(submitEntity)
    .subscribe(
      success => {
        this.resetForm();
    },
      error => console.log("Error!", error)
    )
    
  }

}
