import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdministrativeServiceService {

  public unit: any;
  public detailUnit1: any;
  public detailUnit2: any;
  public subjectProcess: any;
  public status: any;
  public destinationProcess:string;
  public listUnitBeginProcess=[];
  public listUnitEndProcess=[];


  constructor(private _http: HttpClient) {
    this.detailUnit1 = {
      canCreateProcess: false,
      opinion: '',
      dispatch: '',
      filing: '',
      decision: '',
      authorization: '',
      attach: ''
    }
    this.detailUnit2 = {
      canCreateProcess: false,
      opinion: '',
      dispatch: '',
      filing: '',
      decision: '',
      authorization: '',
      attach: ''
    }

  }

  listCanEntityConnectSubjectProcess(key_subjectProcess: string, key_super: string) {
    let url = `${environment.apiUrl}/administrativeSubjectProcess/subjectProcess/${key_subjectProcess}/entity/${key_super}/canConnect`;

    return this._http.get(url);
  }

  filterlistEntityLevelDown(key_super:string,key_subjectProcess:string) {
    
    let url = `${environment.apiUrl}/administrativeSubjectProcess/subjectProcess/${key_subjectProcess}/entity/${key_super}/filter/listEntityLevelDown`;

    return this._http.get(url);

  }
  public getListCityHallUnit() {

    let url = `${environment.apiUrl}/entity/cityHall/listDownEntity`;

    return this._http.get(url);
  }
  public getSubjectProcessToEdit(searchName: string) {

    let url = `${environment.apiUrl}/administrativeSubjectProcess/toEdit?searchName=${searchName}`

    return this._http.get(url);

  }

  public getSubjectProcessT(searchName: string) {

    let url = `${environment.apiUrl}/administrativeSubjectProcess/toEdit?searchName=${searchName}`

    return this._http.get(url);

  }
  public getLisSuperSubjectProcessEndProcess(key_subjectProcess: string) {

    let url = `${environment.apiUrl}/administrativeSubjectProcess/subjectProcess/${key_subjectProcess}/listSuper/endProcess`
    return this._http.get(url);

  }
  public getLisSuperSubjectProcessBeginProcess(key_subjectProcess: string) {

    let url = `${environment.apiUrl}/administrativeSubjectProcess/subjectProcess/${key_subjectProcess}/listSuper/beginProcess`
    return this._http.get(url);

  }

  public getListSubjectProcessListSuper(searchName: string) {

    let url = `${environment.apiUrl}/administrativeSubjectProcess/listSubjectProcess/listSuper?searchName=${searchName}`
    return this._http.get(url);

  }

  public getListSubjectProcessListSuperEndProcess(searchName: string) {

    let url = `${environment.apiUrl}/administrativeSubjectProcess/listSubjectProcess/listSuper/endProcess?searchName=${searchName}`
    return this._http.get(url);

  }

  public listSubjectProcess(searchName: string) {
    let url = `${environment.apiUrl}/subjectProcess?searchName=${searchName}`;

    return this._http.get(url);
  }

  public getListEntityLevelDown(key_entity: string) {

    let url = `${environment.apiUrl}/entity/levelDown/${key_entity}`;

    return this._http.get(url);
  }

  public addBeginUnit(unit:any) {

    let url = `${environment.apiUrl}/administrativeSubjectProcess/beginUnit`;

    return this._http.post(url,{_key:unit._key});

  }

  public addEndUnit(unit:any) {

    let url = `${environment.apiUrl}/administrativeSubjectProcess/endUnit`;

    return this._http.post(url,{_key:unit._key});

  }

  saveCanEntityConnectSubjectProcess(data:any) {
    let _url = `${environment.apiUrl}/administrativeSubjectProcess/canEntityConnectSubjectProcess`;
    
    return this._http.post(_url,data);

  }


}
