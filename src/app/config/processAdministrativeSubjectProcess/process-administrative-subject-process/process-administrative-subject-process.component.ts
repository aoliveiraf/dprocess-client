import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AdministrativeServiceService } from '../administrative-service.service';

@Component({
  selector: 'app-process-administrative-subject-process',
  templateUrl: './process-administrative-subject-process.component.html',
  styles: ['']
})
export class ProcessAdministrativeSubjectProcessComponent implements OnInit {

  constructor(private router:Router, private route: ActivatedRoute,private _service:AdministrativeServiceService) { }

  ngOnInit() {

  }

  callNewEdit() {
      this.router.navigate(['./phase1'],{relativeTo:this.route})
  }

  callPhase2() {

    this.router.navigate(['./phase2'],{relativeTo:this.route})
  }

  callPhase3() {

    this.router.navigate(['./phase3'],{relativeTo:this.route})

  }

}
