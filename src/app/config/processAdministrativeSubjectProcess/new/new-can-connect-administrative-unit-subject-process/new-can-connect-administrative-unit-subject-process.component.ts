import { FormBuilder } from '@angular/forms';
import { AdministrativeServiceService } from './../../administrative-service.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-can-connect-administrative-unit-subject-process',
  templateUrl: './new-can-connect-administrative-unit-subject-process.component.html',
  styles: ['']
})
export class NewCanConnectAdministrativeUNitSubjectProcessComponent implements OnInit {

  _listUnit1 = [];
  _listUnit2 = [];
  _listBeginUnit = [];
  _listEndUnit = [];

  //  _listSubjectProcess = []

  registrationForm = this._fb.group({
    key_unit1: [''],
    key_unit2: ['']
  })


  constructor(private _fb: FormBuilder, private _service: AdministrativeServiceService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getListCityHallUnit1();
    this.getListCityHallUnit2();
    if (this._service.listUnitBeginProcess.length == 0) {
      this.getLisSuperSubjectProcessBeginProcess();
    }
    if (this._service.listUnitEndProcess.length == 0) {

      this.getLisSuperSubjectProcessEndProcess();
    }
    //    this.init();
  }

  getLisSuperSubjectProcessBeginProcess() {

    this._service.getLisSuperSubjectProcessBeginProcess(this._service.subjectProcess._key).subscribe(
      (success: any) => {
        this._listBeginUnit = success;
        this._service.listUnitBeginProcess = success;
        this.updateListUnit1();

      },
      error => console.log('Error!', error)
    )

  }


  getLisSuperSubjectProcessEndProcess() {
    this._service.getLisSuperSubjectProcessEndProcess(this._service.subjectProcess._key).subscribe(
      (success: any) => {
        this._listEndUnit = success;
        this._service.listUnitEndProcess = success;
        this.updateListUnit2();

      },
      error => console.log('Error!', error)
    )

  }

    init() {
  
    }
  
  onDefineBeginProcess(unit: any) {

    this._service.unit = unit;
    this._service.destinationProcess = "beginProcess";
    this.router.navigate(['../phase2'], { relativeTo: this.route })

  }

  onDefineEndProcess(unit: any) {

    this._service.unit = unit;
    this._service.destinationProcess = "endProcess";
    this.router.navigate(['../phase2'], { relativeTo: this.route })

  }

  onAddBegin() {
    this._service.listUnitBeginProcess.push(this._listUnit1.find((element) => {
      return element._key == this.registrationForm.value.key_unit1;
    }))

    this._listUnit1.splice(this._listUnit1.findIndex((element) => { return element._key == this.registrationForm.value.key_unit1 }), 1)

  }

  onAddEnd() {

    this._service.listUnitEndProcess.push(this._listUnit2.find((element) => {
      return element._key == this.registrationForm.value.key_unit2;
    }))

    this._listUnit2.splice(this._listUnit2.findIndex((element) => { return element._key == this.registrationForm.value.key_unit2 }), 1)

  }
  /*
    onchangeUnit1() {
  
      let list = [];
  
      for(let i=0;i<this._listUnit2.length;i++) {
        if(this._listUnit2[i]._key != this.registrationForm.value.key_unit1) {
          list.push(this._listUnit2[i])
        }
      }
      this._service.unit1 = this._listUnit1.find((element:any)=> {
  
        return element._key==this.registrationForm.value.key_unit1;
      }) 
  
      this._listUnit2=list;    
    }
  
    onchangeUnit2() {
  
      let list = [];
  
      for(let i=0;i<this._listUnit1.length;i++) {
        if(this._listUnit1[i]._key != this.registrationForm.value.key_unit2) {
          list.push(this._listUnit1[i])
        }
      }
      this._service.unit2 = this._listUnit2.find((element:any)=> {
  
        return element._key==this.registrationForm.value.key_unit2;
      }) 
  
      this._listUnit1=list;    
    }
  */
  getListCityHallUnit1() {

    this._service.getListCityHallUnit().subscribe(
      (success: any) => {
        this._listUnit1 = success;
      },
      error => console.log('Error!', error)
    )
  }

  updateListUnit1() {

    this._service.listUnitBeginProcess.forEach((element1)=>{
      console.log(element1);
      
      this._listUnit1.splice(this._listUnit1.findIndex((element) => { return element._key == element1._key }), 1)
  
    })    
  }

  updateListUnit2() {

    this._service.listUnitEndProcess.forEach((element2)=>{
      
      this._listUnit2.splice(this._listUnit2.findIndex((element) => { return element._key == element2._key }), 1)
  
    })    
  }

  getListCityHallUnit2() {

    this._service.getListCityHallUnit().subscribe(
      (success: any) => {
        this._listUnit2 = success;
        this.updateListUnit2();
      },
      error => console.log('Error!', error)
    )
  }


}
