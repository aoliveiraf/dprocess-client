import { Component, OnInit } from '@angular/core';
import { AdministrativeServiceService } from '../administrative-service.service';

@Component({
  selector: 'app-edit-can-connect-administrative-subject-process',
  templateUrl: './edit-can-connect-administrative-subject-process.component.html',
  styles: ['']
})
export class EditCanConnectAdministrativeSubjectProcessComponent implements OnInit {

  constructor(private _service:AdministrativeServiceService) { }
  ngOnInit() {
  }

}
