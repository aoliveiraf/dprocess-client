import { SubjectService } from './../../../service/subject.service';
import { ListEntityService } from './../../../service/list-entity.service';
import { AuthService } from 'src/app/service/auth.service';
import { FormBuilder } from '@angular/forms';
import { AdministrativeServiceService } from './../administrative-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-phase2',
  templateUrl: './phase2.component.html',
  styles: ['']
})
export class Phase2Component implements OnInit {

  constructor(private router: Router, private route: ActivatedRoute, private _service: AdministrativeServiceService, private _fb: FormBuilder, private _auth: AuthService, private _serviceEntity: ListEntityService, private _serviceSubjectProcess: SubjectService) { }

  ngOnInit() {

    this.listCanEntityConnectSubjectProcess();
    this.listAdministrative();

  }
  status: string = 'creating';

  entityDetailSelected: any;
  registrationForm = this._fb.group({
    id_subjectProcess: [''],
    id_entity: [''],
    suggestionText: [''],
    canCreateProcess: [false],
    opinion: [''],
    dispatch: [''],
    filing: [''],
    decision: [''],
    authorization: [''],
    attach: ['']
  })
  _listSubjectProcess: any;
  _listAdministrativeSibling: any;
  _listCanEntityConnectSubjectProcess: any;
  //  id_subjectProcess:string;

  /*  listEntityLevelDown() {
  
      this._service.getListEntityLevelDown(key_entity)
        .subscribe(
          success => {
            this._listCanEntityConnectSubjectProcess = success
          },
          error => console.log("Error!", error)
  
        )
    }
  */



  listCanEntityConnectSubjectProcess() {


    this._service.listCanEntityConnectSubjectProcess(this._service.subjectProcess._key, this._service.unit._key)
      .subscribe(
        success => {
          console.log('listCanEntityConnectSubjectProcess');
          
          this._listCanEntityConnectSubjectProcess = success
          this.listAdministrative();
        },
        error => console.log("Error!", error)

      )
  }
  listSubjectProcess() {
    this._serviceSubjectProcess.listSubjectProcess('').subscribe(
      success => this._listSubjectProcess = success,
      error => console.log('Error!', error)
    )

  }


  listAdministrativeResult(listAdministrativeSibling: any[]) {

    let key_entity = this._service.unit._key;

    this._service.getListEntityLevelDown(key_entity).subscribe(
      (success: any) => {
        this.listAdministrativeResult(success);
      },
      error => console.log('Error!', error)
    )


/*    this._listAdministrativeSibling = []
    for (let i = 0; i < listAdministrativeSibling.length; i++) {
      let j = 0;
      for (; j < this._listCanEntityConnectSubjectProcess.length &&
        this._listCanEntityConnectSubjectProcess[j].entity._key != listAdministrativeSibling[i]._key; j++) {
      }
      if (j == this._listCanEntityConnectSubjectProcess.length)
        this._listAdministrativeSibling.push(listAdministrativeSibling[i]);
    }
    */


  }

  listAdministrative() {
    let key_super = this._auth.getDataLogged().super._key;
    let key_subjectProcess:string = this._service.subjectProcess._key;
    this._service.filterlistEntityLevelDown(key_super,key_subjectProcess).subscribe(
      (success: any) => {
        console.log('listAdministrative');
        
        console.log(success);
        
        this._listAdministrativeSibling = success;
      },
      error => console.log('Error!', error)
    )

  }


  edit(entityDetail: any) {
    this.status = 'editing';

    this.registrationForm.setValue({
      id_subjectProcess: this.registrationForm.value.id_subjectProcess,
      id_entity: entityDetail.entity._key,
      suggestionText: entityDetail.has.suggestionText || '',
      canCreateProcess: entityDetail.has.canCreateProcess,
      opinion: entityDetail.has._listTypeDocumentCreation.indexOf('Opinion') >= 0 ? 'Opinion' : '',
      dispatch: entityDetail.has._listTypeDocumentCreation.indexOf('Dispatch') >= 0 ? 'Dispatch' : '',
      filing: entityDetail.has._listTypeDocumentCreation.indexOf('Filing') >= 0 ? 'Filing' : '',
      decision: entityDetail.has._listTypeDocumentCreation.indexOf('Decision') >= 0 ? 'Decision' : '',
      authorization: entityDetail.has._listTypeDocumentCreation.indexOf('Authorization') >= 0 ? 'Authorization' : '',
      attach: entityDetail.has._listTypeDocumentCreation.indexOf('Attach') >= 0 ? 'Attach' : ''

    })
    this.entityDetailSelected = entityDetail;

    this.show("collapseExample")

    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;


  }

  show(id) {
    var x = document.getElementById(id);

    x.style.display = "block";
  }

  showORhide(id) {
    var x = document.getElementById(id);

    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  newCan() {

    this.status = 'creating';
    this.registrationForm.setValue({
      id_subjectProcess: this._service.subjectProcess._key,
      id_entity: '',
      suggestionText: '',
      canCreateProcess: false,
      opinion: '',
      dispatch: '',
      filing: '',
      decision: '',
      authorization: '',
      attach: ''
    })

  }

  onSubmit() {
    let _listTypeDocumentCreation = [];
    if (this.registrationForm.value.opinion == "Opinion") {
      _listTypeDocumentCreation.push("Opinion");
    }
    if (this.registrationForm.value.dispatch == "Dispatch") {
      _listTypeDocumentCreation.push("Dispatch");
    }
    if (this.registrationForm.value.filing == "Filing") {
      _listTypeDocumentCreation.push("Filing");
    }
    if (this.registrationForm.value.decision == "Decision") {
      _listTypeDocumentCreation.push("Decision");
    }
    if (this.registrationForm.value.authorization == "Authorization") {
      _listTypeDocumentCreation.push("Authorization");
    }
    if (this.registrationForm.value.canCreateProcess == "true") {
      _listTypeDocumentCreation.push("Ordinary");
    }
    if (this.registrationForm.value.attach == "Attach") {
      _listTypeDocumentCreation.push("Attach");
    }

    let submitData: any = {
      id_subjectProcess: this._service.subjectProcess._key,
      id_entity: this.registrationForm.value.id_entity,
      canCreateProcess: this.registrationForm.value.canCreateProcess,
      _listTypeDocumentCreation: _listTypeDocumentCreation,
      destinationProcess:this._service.destinationProcess
    }
    if (this.status === 'creating') {
      this._service.saveCanEntityConnectSubjectProcess(submitData).subscribe(
        sucess => {
          //        this.listSubjectProcess();
          this.listCanEntityConnectSubjectProcess();
        },
        error => console.log('Error!', error)
      )
    } else {
      submitData.key_can = this.entityDetailSelected.has._key;
      this._serviceEntity.updateCanEntityConnectSubjectProcess(submitData).subscribe(
        sucess => {
          //        this.listSubjectProcess();
          this.listCanEntityConnectSubjectProcess();
        },
        error => console.log('Error!', error)
      )

    }

    this.newCan();
    //    this.listCanEntityConnectSubjectProcess();
    //   this.listAdministrativeSibling();

  }

  connectUnit() {

    this.router.navigate(['../choiceAdministrativeUnit'], { relativeTo: this.route })

  }

  callNewEdit() {
    this.router.navigate(['../phase1'],{relativeTo:this.route})
}


}

