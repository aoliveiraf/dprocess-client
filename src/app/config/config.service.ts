import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(private _http:HttpClient) { }

  public getSubjectProcess(searchName:string) {

    let url = `${environment.apiUrl}/administrativeSubjectProcess?searchName=${searchName}`

    return this._http.get(url);

  }

}
