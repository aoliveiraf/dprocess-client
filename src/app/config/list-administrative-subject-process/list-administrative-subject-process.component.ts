import { Component, OnInit } from '@angular/core';
import { AdministrativeServiceService } from '../processAdministrativeSubjectProcess/administrative-service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list-administrative-subject-process',
  templateUrl: './list-administrative-subject-process.component.html',
  styles: ['']
})
export class ListAdministrativeSubjectProcessComponent implements OnInit {

  _listSubjectProcess = [];
  constructor(private _service: AdministrativeServiceService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.listSubjectProcess("")
    this._service.listUnitBeginProcess = []
    this._service.listUnitEndProcess = []

  }

  listSubjectProcess(partName: string) {

    this._service.getListSubjectProcessListSuper(partName)
      .subscribe(
        (success: any) => {
          this._listSubjectProcess = success;

        },
        error => console.log('Error!', error)
      )

  }


  onEditNew(detail: any) {

    this._service.subjectProcess = detail.subjectProcess;

//    this._service.unit1 = detail.listSuper[0];
//    this._service.unit2 = detail.listSuper[1];
       
      this.router.navigate(['../processAdministrativeSubjectProcess/phase1'], { relativeTo: this.route })

  }


}
