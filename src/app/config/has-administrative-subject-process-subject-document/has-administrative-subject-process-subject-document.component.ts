import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ConfigService } from '../config.service';
import { SubjectService } from 'src/app/service/subject.service';

@Component({
  selector: 'app-has-administrative-subject-process-subject-document',
  templateUrl: './has-administrative-subject-process-subject-document.component.html',
  styles: ['']
})
export class HasAdministrativeSubjectProcessSubjectDocumentComponent implements OnInit {

  constructor(private _fb: FormBuilder, private _service: ConfigService,private _serviceSubjectProcess: SubjectService) { }

  subjectProcessForm = this._fb.group({
    id_subjectProcess: [''],
    id_subjectDocument: ['']
  })
  _listSubjectProcess: any;
  _listSubjectDocument: any;
  _listHasSubjectDocument: any = [];
  //  id_subjectProcess:string;

  ngOnInit() {
    this.listSubjectProcess();
    //    this.listSubjectDocument();
  }

  onChangeSubjectProcess() {
    this.listSubjectDocument();
    this.listHasSubjectDocument();
  }

  listSubjectProcess() {
    this._service.getSubjectProcess('').subscribe(
      success => {
      this._listSubjectProcess = success;
      },
      error => {
        console.log('Error!', error)

      }
    )

  }

  listSubjectDocument() {
    this._serviceSubjectProcess.listSubjectDocumentDiffernce(this.subjectProcessForm.value.id_subjectProcess).subscribe(
      (success) => {

        this._listSubjectDocument = success
        this._listSubjectDocument = this._listSubjectDocument.sort(function (a, b) {
          return ((a.name == b.name) ? 0 : ((a.name > b.name) ? 1 : -1));
        });
      },
      error => {
        console.log('Error!', error)
      }
    )

  }

  listHasSubjectDocument() {
    this._serviceSubjectProcess.listHasSubjectDocument(this.subjectProcessForm.value.id_subjectProcess).subscribe(
      success => {
      this._listHasSubjectDocument = success;
      },
      error => {
        console.log('Error!', error);
      }
    )

  }

  delete(subject: any) {
    this._serviceSubjectProcess.deleteHasSubjectProceesSubject({ key_subjectProcess: this.subjectProcessForm.value.id_subjectProcess, key_subjectDocument: subject._key }).
      subscribe(
        success => {
          this.listHasSubjectDocument();
        },
        error => console.log('error', error)

      )
  }
  onSubmit() {
    this._serviceSubjectProcess.saveHasSubjectProcessSubject(this.subjectProcessForm.value).subscribe(
      sucess => {
        this.listSubjectProcess();
        this.listSubjectDocument();
        this.listHasSubjectDocument();
      },
      error => console.log('Error!', error)

    )
  }

}
