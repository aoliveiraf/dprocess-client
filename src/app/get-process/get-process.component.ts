import { Router } from '@angular/router';
import { MovementProcessService } from './../service/movement-process.service';
import { AuthService } from './../service/auth.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { EntityService } from '../service/entity.service';

@Component({
  selector: 'app-get-process',
  templateUrl: './get-process.component.html',
  styles: ['']
})
export class GetProcessComponent implements OnInit {

  _listProcess: any;
  listEntity: any;
  idEntityGetter: string;
  listEntityCanMoveToSubject: any;
  isShowDispatch: boolean = false;
  listIdProcessToMove = [];
  btnDisabled = true;
  constructor(private _router: Router, private _fb: FormBuilder, private _entityService: EntityService, private _movement: MovementProcessService, private _auth: AuthService) { }

  registrationForm = this._fb.group({
    id_entityFrom: [''],
    id_process: ['']
  })
  ngOnInit() {
    this.lookupLevelDown();

  }

  lookupLevelDown() {

    this._entityService.listLevelDown(this._auth.getDataLogged().super._key).
      subscribe(
        (success: any) => {
          let le = success;
          this.listEntity = le.filter((value, index, arr) => {

            return value._key != this._auth.getDataLogged().entity._key;

          });
        },
        error => console.log("Error!", error)
      )
  }

  dispatch() {
    this.isShowDispatch = !this.isShowDispatch;
  }
  listProcess() {
    let data = { id_entity: this.registrationForm.value.id_entityFrom }
    this._entityService.listProcessFromEntity(data)
      .subscribe(
        success => {
          this._listProcess = success;
          console.log(this._listProcess);

        },
        error => console.log("Error!", error)
      )
  }

  onChangeCheck(id_process) {
    let index = this.listIdProcessToMove.indexOf(id_process);
    if (index == -1) {
      this.listIdProcessToMove.push(id_process);
    } else {
      this.listIdProcessToMove.splice(index, 1)
    }
    if(this.listIdProcessToMove.length>0) {
      this.btnDisabled=false;
    } else {
      this.btnDisabled=true;
    }
  }

  onChange() {
    this.listProcess();
  }
  reset() {
    this.registrationForm.setValue({
      id_entityFrom:'',
      id_process:''  
    })
    this.listIdProcessToMove=[]

  }

  move() {
    let idEntityFrom = this.registrationForm.value.id_entityFrom;
    let idEntityTo = this._auth.getDataLogged().entity._key;
    let idSuper = this._auth.getDataLogged().super._key;

    let data = { id_super: idSuper, listIdProcess: this.listIdProcessToMove, id_entityFrom: idEntityFrom, id_entityTo: idEntityTo }
    this._movement.getListProcess(data)
      .subscribe(
        success => {
          this.listProcess();
          this.reset()
        },
        error => console.log('error', error)
      )
  }

  destacarId() {
    console.log(this.registrationForm.value.id_process)
    this.filterSelection(this.registrationForm.value.id_process)
  }

  filterSelection(query: string) {
    var elements, i;
    elements = document.getElementsByClassName("dinamic-show");

    // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
    for (i = 0; i < elements.length; i++) {
      this.removeClass(elements[i], query);
    }
  }

  // Hide elements that are not selected
  removeClass(element, query) {

    let index = element.className.indexOf(query);

    if (index < 0) {
      element.style.display = "none"
    } else {
      element.style.display = "block"
    }

  }

}
