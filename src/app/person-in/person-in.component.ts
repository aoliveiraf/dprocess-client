import { AuthService } from './../service/auth.service';
import { FormBuilder } from '@angular/forms';
import { ListEntityService } from './../service/list-entity.service';
import { Component, OnInit } from '@angular/core';
import { NewPersonService } from '../service/new-person.service';

@Component({
  selector: 'app-person-in',
  templateUrl: './person-in.component.html',
  styles: []
})
export class PersonInComponent implements OnInit {

  listEntity:any = [];
  currentEntity:any;
  constructor(private _auth: AuthService,private _fb:FormBuilder,private serviceListEntity:ListEntityService, private serviceNewPerson:NewPersonService) { }

  registrationForm = this._fb.group({
    name: [''],
    id: [''],
    id_creator: [''],
    id_super: ['']
  });
  
  ngOnInit() {
  }

  onKeyUp() {
    this.serviceListEntity.listAdministrative(this.registrationForm.value.id_super)
    .subscribe(
      success=> this.listEntity=success,
      error=> console.log("error!",error)
    )
    
  }

  onEntityChange() {
    this.currentEntity = this.getSelecteEntitytByName(this.registrationForm.value.id_super);
    this.updateForm(this.currentEntity);
  }

  updateForm(currentEntity:any) {
    this.registrationForm.setValue({
      name: this.registrationForm.value.name,
      id: this.registrationForm.value.id,
      id_creator: this.registrationForm.value.id_creator,
      id_super: this.currentEntity.name
    });

  }

  getSelecteEntitytByName(selectedKey: string): any {
    return this.listEntity.find(entity => entity._key === selectedKey);
  }

  onSubmit() {
    let dataLogged = this._auth.getDataLogged();

    let submitEntity = this.registrationForm.value;
    submitEntity.id_super = this.currentEntity._key;
    submitEntity.id_creator = dataLogged.entity._key;
    
    this.serviceNewPerson.sendIndividualPersonIn(submitEntity)
    .subscribe(
      success => console.log("Success!",success),
      error => console.log("Error!", error)
    )
  }

}
