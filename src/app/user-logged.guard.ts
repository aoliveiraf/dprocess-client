import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserLoggedGuard implements CanActivate{
  
  constructor(private _authService: AuthService) { }

  canActivate(): boolean {
    if (!!this._authService.getDataLogged()) {
        return true;
    } else {
        return false;
    }
  }
}
