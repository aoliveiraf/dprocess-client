import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { EntityService } from '../service/entity.service';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-pending-process',
  templateUrl: './pending-process.component.html',
  styles: ['']
})
export class PendingProcessComponent implements OnInit {

  listDetailProcess:any;
   
  constructor(private route:ActivatedRoute, private _fb:FormBuilder, private _serviceEntity:EntityService, private _auth:AuthService) { }

  registrationForm = this._fb.group({
    id_process:['']
  })

  ngOnInit() {
    let id_entity=this.route.snapshot.paramMap.get("id_entity");

    this._serviceEntity.fetchListProcessDetailPending(id_entity)
    .subscribe(
      success => {
        this.listDetailProcess = success;
        
      },
      error => console.log('Error!', error)
    )
  }

  onReceive(processDetail:any) {
    
    this._serviceEntity.receiveProcess({idProcess:processDetail.process._key,
                                      idEntity:this._auth.getDataLogged().entity._key,
                                      idSuper:this._auth.getDataLogged().super._key,
                                      idEvent:processDetail.event._key})
    .subscribe(
      success => {
        this._serviceEntity.fetchListProcessDetailPending(this._auth.getDataLogged().super._key)
        .subscribe(
          success => this.listDetailProcess = success,
          error => console.log('Error!', error)
        )
      },
      error => console.log('Error!',error)
      
      
    )
    
  }

  onReject(processDetail:any) {
    
    this._serviceEntity.rejectProcess({idProcess:processDetail.process._key,
                                      idEntity:this._auth.getDataLogged().entity._key,
                                      idSuper:this._auth.getDataLogged().super._key,
                                      idEvent:processDetail.event._key})
    .subscribe(
      success => {
        this._serviceEntity.fetchListProcessDetailPending(this._auth.getDataLogged().super._key)
        .subscribe(
          success => this.listDetailProcess = success,
          error => console.log('Error!', error)
        )
    
      },
      error => console.log('Error!',error)
    )
    
  }

  showORhide(id) {
    console.log(id);
    
    var x = document.getElementById("list"+id);

    if (x.style.display === "none") {
      x.style.display = "block";
    } else {
      x.style.display = "none";
    }
  }

  destacarId() {
    console.log(this.registrationForm.value.id_process)
    this.filterSelection(this.registrationForm.value.id_process)
  }

  filterSelection(query:string) {
    var elements, i;
    elements = document.getElementsByClassName("dinamic-show");
    
    // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
    for (i = 0; i < elements.length; i++) {
      this.removeClass(elements[i], query);
    }
  }
    
  // Hide elements that are not selected
  removeClass(element, query) {

    let index = element.className.indexOf(query);
    
    if(index < 0) {
      element.style.display="none"
    } else {
      element.style.display="block"
    }

  }


}
