import { Component, OnInit, Input } from '@angular/core';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-button-alert',
  templateUrl: './button-alert.component.html',
  styles: ['']
})
export class ButtonAlertComponent implements OnInit {

  private _success = new Subject<string>();
  @Input() public textButton:string;
  staticAlertClosed = false;
  successMessage:string;
  @Input() public message: string;
  @Input() public duration: number;
  @Input() public type: string;
  @Input() public disabled: string;

  ngOnInit(): void {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(this.duration)
    ).subscribe(() => this.successMessage = null);
  }

  public changeSuccessMessage() {
    this._success.next(this.message);
  }
}
