import { AuthService } from './service/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthAdministratorGuard implements CanActivate {
  constructor(private _authService: AuthService, private _router: Router) { }

  canActivate(): boolean {
    if (!!this._authService.getDataLogged()) {
      let user = this._authService.getDataLogged().user;
      if (user.listType.indexOf('Administrator') >= 0) {
        return true;
      } else {
        return false;
      }
    } else {
      this._router.navigate(['/login']);
      return false;
    }
  }
}
