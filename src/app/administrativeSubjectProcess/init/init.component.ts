import { Router, ActivatedRoute } from '@angular/router';
import { ServiceService } from './../service.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-init',
  templateUrl: './init.component.html',
  styles: ['']
})
export class InitComponent implements OnInit {

  constructor(private router:Router,private route:ActivatedRoute, private _auth: AuthService, private _fb: FormBuilder,private _service: ServiceService) { }
  status: string = "creating";
  listing:boolean = false;
  _submit: boolean = false;

  subjectProcessForm = this._fb.group({
    name: [''],
    _key: ['']
  })
  _listSubjectProcess: any;

  ngOnInit() {
    this.listSubjectProcess('');
    console.log(this._listSubjectProcess);
    
  }

  listSubjectProcess(partName: string) {
    
    this._service.getSubjectProcess(partName,this.status).subscribe(
      (success: any) => {
        this._listSubjectProcess = success;
        if(success.length >0) {
          this.listing = true;
        } else {
          this.listing = false;
        }
        this.testDisabled(success.length)
      },
      error => console.log('Error!', error)
    )
  }

  testDisabled(lengthList:number) {

    if (lengthList > 0) {
      this._submit = false;
    } else if(this.subjectProcessForm.valid){
      this._submit = true;
    }
  }

  onKeyUpName() {

    this.listSubjectProcess(this.subjectProcessForm.value.name);
  }

  changingName() {

    this.listSubjectProcess(this.subjectProcessForm.value.name);
  }
  /*
  delete(subjectProcess: any) {
    this.status='creating';
    this._serviceSubjectProcess.delete(subjectProcess._key).subscribe(
      (success: any) => {
        this.listSubjectProcess(this.subjectProcessForm.value.name);
      },
      error => console.log('Error!', error)
    )
  }
*/
  onEdit(subjectProcess: any) {
    this.status = 'editing';
    this.subjectProcessForm.setValue({
      name: subjectProcess.name,
      _key: subjectProcess._key
    })
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  connectUnit(subject: any) {
    this._service.subjectProcess = subject;
    
    this.router.navigate(['../choiceAdministrativeUnit'],{relativeTo:this.route})
  
  }

  onNew() {
    this.status = 'creating';
    this.subjectProcessForm.setValue({
      name:'',
      _key:''
    })
  }

  onSubmit() {
    let submitData = this.subjectProcessForm.value;
    this._service.save(submitData,this.status)
    .subscribe(
      (success: any) => {
        this.onNew();
        this.listSubjectProcess(success.name);
      },
      error => console.log('Error!', error)

    )
  }
}
