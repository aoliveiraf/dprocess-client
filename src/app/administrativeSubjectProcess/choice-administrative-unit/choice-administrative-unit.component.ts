import { Router, ActivatedRoute } from '@angular/router';
import { ServiceService } from './../service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-choice-administrative-unit',
  templateUrl: './choice-administrative-unit.component.html',
  styles: ['']
})
export class ChoiceAdministrativeUnitComponent implements OnInit {

  constructor(private _service:ServiceService,private router:Router, private route:ActivatedRoute) { }
  
  listUnit:[]

  ngOnInit() {
    this.getListCityHallUnit();    
  }

  getListCityHallUnit() {
    
    this._service.getListCityHallUnit().subscribe(
      (success: any) => {
        this.listUnit = success;
      },
      error => console.log('Error!', error)
    )
  }

  choice(entity:any) {

    this._service.entityBeginEnd = entity;
    this._service.entityMedium = entity;

    
    this.router.navigate(['../canConnectSubjectProcess'],{relativeTo:this.route})

  }
}
