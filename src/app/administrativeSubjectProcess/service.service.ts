import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  public subjectProcess:any;
  public entityBeginEnd:any;
  public entityMedium:any;
  
  constructor(private _http:HttpClient) { }

  public save(data:any,status:string) {
    const url = `${environment.apiUrl}/administrativeSubjectProcess`;

    if (status === 'editing') {
      return this._http.put(url, data);
    } else {

      return this._http.post(url, data);
    }

  }

  public getListCityHallUnit() {

    let url = `${environment.apiUrl}/entity/cityHall/listDownEntity`;

    return this._http.get(url);
  }
  public getSubjectProcess(searchName:string,status:string) {

    let url = `${environment.apiUrl}/administrativeSubjectProcess?searchName=${searchName}`
//    if(status=='editing')
//      url = `${environment.apiUrl}/administrativeSubjectProcess/fit?searchName=${searchName}`
    return this._http.get(url);

  }

  public listSubjectProcess(searchName: string) {
    let url = `${environment.apiUrl}/subjectProcess?searchName=${searchName}`;

    return this._http.get(url);
  }

  public getListEntityLevelDown(key_entity:string) {

    let url = `${environment.apiUrl}/entity/levelDown/${key_entity}`;

    return this._http.get(url);
  }



}
