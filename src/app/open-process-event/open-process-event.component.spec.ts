import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenProcessEventComponent } from './open-process-event.component';

describe('OpenProcessEventComponent', () => {
  let component: OpenProcessEventComponent;
  let fixture: ComponentFixture<OpenProcessEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenProcessEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenProcessEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
