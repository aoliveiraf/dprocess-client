import { ListEntityService } from './../service/list-entity.service';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NewOpenProcessEventService } from '../service/new-open-process-event.service';

@Component({
  selector: 'app-open-process-event',
  templateUrl: './open-process-event.component.html',
  styleUrls: ['./open-process-event.component.css']
})
export class OpenProcessEventComponent implements OnInit {

  listEntity:any = [];
  currentEntity:any;
  constructor(private _fb:FormBuilder,private serviceListEntity:ListEntityService, private serviceOpenProcessEvent:NewOpenProcessEventService) { }

  registrationForm = this._fb.group({
    id_creator:['15090'],
    id_process:['94477'],
    id_entity:['']  
  });
  
  ngOnInit() {
  }

  onKeyUp() {
    this.serviceListEntity.listAdministrative(this.registrationForm.value)
    .subscribe(
       success=> this.listEntity=success,
      error=> console.log("error!",error)
    )
    
  }

  onEntityChange() {
    this.currentEntity = this.getSelecteEntitytByName(this.registrationForm.value.id_entity);
    this.updateForm();
  }

  updateForm() {
    this.registrationForm.setValue({
      id_creator:this.registrationForm.value.id_creator,
      id_process:this.registrationForm.value.id_process,
      id_entity:this.currentEntity.name  
    });

  }

  getSelecteEntitytByName(selectedKey: string): any {
    return this.listEntity.find(entity => entity._key === selectedKey);
  }

  onSubmit() {
    let submitEntity = this.registrationForm.value;
    submitEntity.id_entity = this.currentEntity._key;

    this.serviceOpenProcessEvent.send(submitEntity)
    .subscribe(
      success => console.log("Success!",success),
      error => console.log("Error!", error)
    )
  }
}
